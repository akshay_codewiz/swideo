//
//  SelectContactTimeVC.m
//  Swideo
//
//  Created by akshay on 7/14/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "SelectContactTimeVC.h"
#import "AgeCell.h"
@interface SelectContactTimeVC ()

@end

@implementation SelectContactTimeVC
@synthesize arrTimeSelected ,isWeekDay;
- (void)viewDidLoad {
    [super viewDidLoad];
    if (arrTimeSelected.count <=0) {
        [self getTimeAPi];
    }
    NSLog(@"arrTimeSelected--%@",arrTimeSelected);
    // Do any additional setup after loading the view.
}
#pragma mark - Validate User name
-(void)getTimeAPi
{
    arrTimeSelected = [[NSMutableArray alloc]init];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    if (_isEdit) {
        [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    }
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,timeRangeListPAI] withTag:timeRangeListPAITag withParameter:dict];
    apiObject.delegate = self ;
    
}


#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    
    switch (tagCon) {
        case timeRangeListPAITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                
                arrTimeSelected = [NSMutableArray arrayWithArray:[dictResponse valueForKey:@"data"]];
                [tblView reloadData];
            }
            
            
            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            break ;
            
        case updateUserProfileAPITag:
        {
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                
                
                [self.navigationController popViewControllerAnimated:true];
            }
            
            
            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            
            break ;
        default:
            break;
            
    }
    
}
- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrTimeSelected count] ;
    
}

- (CGFloat)tableView:(UITableView *)tableView
estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
#pragma mark
#pragma mark - TableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AgeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Age" forIndexPath:indexPath];
    cell.lblGender.text =[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"name"];
    [cell.imgCheck setHidden:true];
    
    if (isWeekDay) {
        if ([[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"isWeekday"] isEqualToString:@"1"]) {
            [cell.imgCheck setHidden:false];
            
        }
    }
    else{
        if ([[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"isWeekend"] isEqualToString:@"1"]) {
            [cell.imgCheck setHidden:false];
            
        }
    }
    
    
    //    for (int i = 0;i <[arrGenderSelected count]; i++) {
    //        NSString *strID =[[arrGender   valueForKey:@"genderId"] objectAtIndex:indexPath.row];
    //        NSString *selectedID = [arrGenderSelected objectAtIndex:i];
    //        if ([strID isEqualToString:selectedID]) {
    //          [cell.imgCheck setHidden:false];
    //        }
    //
    //    }
    return  cell ;
    
    
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *isLike ;
    if (isWeekDay) {
       isLike = [NSString stringWithFormat:@"%@",[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"isWeekday"]];
    }
    else{
        isLike = [NSString stringWithFormat:@"%@",[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"isWeekend"]];

    }
    
    if ([isLike isEqualToString:@"0"])
    {
        isLike =@"1";
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
        [dict setValue:[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"name"] forKey:@"name"];
        [dict setValue:[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"timerangeId"] forKey:@"timerangeId"];
        [dict setValue:[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"groupRange"] forKey:@"groupRange"];
        if (isWeekDay) {
            [dict setValue:isLike forKey:@"isWeekday"];
            [dict setValue:[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"isWeekend"] forKey:@"isWeekend"];
            
        }
        else{
            [dict setValue:isLike forKey:@"isWeekend"];
            [dict setValue:[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"isWeekday"] forKey:@"isWeekday"];
            
        }
        [arrTimeSelected replaceObjectAtIndex:indexPath.row withObject:dict];
    }
    else
    {
        isLike =@"0";
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
        [dict setValue:[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"name"] forKey:@"name"];
        [dict setValue:[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"timerangeId"] forKey:@"timerangeId"];
        [dict setValue:[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"groupRange"] forKey:@"groupRange"];
        
        if (isWeekDay) {
            [dict setValue:isLike forKey:@"isWeekday"];
            [dict setValue:[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"isWeekend"] forKey:@"isWeekend"];

        }
        else{
            [dict setValue:isLike forKey:@"isWeekend"];
            [dict setValue:[[arrTimeSelected objectAtIndex:indexPath.row] valueForKey:@"isWeekday"] forKey:@"isWeekday"];

        }
        
        [arrTimeSelected replaceObjectAtIndex:indexPath.row withObject:dict];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [tblView reloadData];
        
    });
    
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)doneAction:(id)sender {
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"flag ==  %@", @"1"];
    
    if (_isEdit) {
        
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
        
        NSPredicate *predicateweekend = [NSPredicate predicateWithFormat:@"isWeekend ==  %@", @"1"];
        NSPredicate *predicateweekday = [NSPredicate predicateWithFormat:@"isWeekday ==  %@", @"1"];
        if (isWeekDay) {
            if ([NSMutableArray arrayWithArray:[arrTimeSelected filteredArrayUsingPredicate:predicateweekday]].count > 0) {
                [ dict setValue:[GlobalMethods getJsonFromArray:[[NSMutableArray arrayWithArray:[arrTimeSelected filteredArrayUsingPredicate:predicateweekday]] valueForKey:@"timerangeId"]] forKey:@"Weekday"];
            }
        }
        else{
            if ([NSMutableArray arrayWithArray:[arrTimeSelected filteredArrayUsingPredicate:predicateweekend]].count > 0) {
                [dict setValue:[GlobalMethods getJsonFromArray:[[NSMutableArray arrayWithArray:[arrTimeSelected filteredArrayUsingPredicate:predicateweekend]] valueForKey:@"timerangeId"]] forKey:@"Weekend"];
            }
            
        }
        

        //
        [dict setValue:[[UserDefaultHelper sharedDefaults]getUserID] forKey:@"userId"];
        CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,updateUserProfileAPI] withTag:updateUserProfileAPITag withParameter:dict];
        apiObject.delegate = self ;
        
    }
    else{
        [self.timeSelectiondelegate SelectContactTImeDelegate:arrTimeSelected ];
        [self.navigationController popViewControllerAnimated:true];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
