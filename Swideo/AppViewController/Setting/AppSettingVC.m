//
//  AppSettingVC.m
//  Swideo
//
//  Created by akshay on 7/15/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "AppSettingVC.h"
#import "AppSettingCell.h"
#import "AppSettingDetailCell.h"
#import "AppSettingLogOutCell.h"
#import "CMSViewController.h"
@interface AppSettingVC ()

@end

@implementation AppSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark
#pragma mark - TableView DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50 ;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    NSArray * arrSection =[[NSArray alloc]initWithObjects:@"Notifications",@"Legal",@"",@"", nil];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 55)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, tableView.frame.size.width, 40)];
    [label setFont:[UIFont boldSystemFontOfSize:14]];
    NSString *string =[arrSection objectAtIndex:section];
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:APPGRAYCOLOR]; //your background color...
    
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0.0f, 0.2f);
    view.layer.shadowOpacity = 0.8f;
    view.layer.shadowPath = shadowPath.CGPath;
    
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            return 4 ;
        }
            break;
        case 1:
        {
            return 2 ;
        }
            break;
        case 2:
        {
            return 1;

        }
            break;
        case 3:
        {
            return 1;
        }
            break;
            
        default:
            return 0 ;

            break;
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4 ;

}// Default is 1 if not implemented


//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    float fixheight = 60 ;
//    
//    switch (indexPath.row) {
//        case 0:
//        {
//            return  fixheight ;
//            
//        }
//        case 1:
//        {
//            return  fixheight ;
//            
//        }
//        case 2:
//        {
//            return  150 ;
//        }
//        case 3:
//        {
//            return  150 ;
//        }
//            
//        default:
//            return  fixheight ;
//            break ;
//            
//            
//    }
//    
//    
//    
//}

#pragma mark
#pragma mark - TableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.section) {
            // Push ,new matches , upcoming , in App vibrations

        case 0:
        {
            switch (indexPath.row) {

           case 0:
        {
            AppSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AppSettingCell" forIndexPath:indexPath];
            cell.lblTitle.text = @"Push Notifications";
            [cell.switchOnOff addTarget:self action:@selector(switchNotification:) forControlEvents:UIControlEventValueChanged];
            return  cell;
            
            
            
        }
            break;
        case 1:
        {
            AppSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AppSettingCell" forIndexPath:indexPath];
            cell.lblTitle.text = @"New Matches";
            [cell.switchOnOff addTarget:self action:@selector(switchMatches:) forControlEvents:UIControlEventValueChanged];

            return  cell;
            
        }
            break;
        case 2:
        {
            AppSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AppSettingCell" forIndexPath:indexPath];
            cell.lblTitle.text = @"Upcoming Calls";
            [cell.switchOnOff addTarget:self action:@selector(switchCalls:) forControlEvents:UIControlEventValueChanged];

            return  cell;
            

            
            
        }
            break;
        case 3:
        {
            AppSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AppSettingCell" forIndexPath:indexPath];
            cell.lblTitle.text = @"In-app Vibrations";
            [cell.switchOnOff addTarget:self action:@selector(switchVibrate:) forControlEvents:UIControlEventValueChanged];

            return  cell;
            
            
            
            
        }
            break;
            
        default:
            break;
    }
      }
            break ;
            // Privecy policy , terms of services
    case 1:
    {
        switch (indexPath.row) {
            case 0:
            {
                AppSettingDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AppSettingDetailCell" forIndexPath:indexPath];
                cell.lblTitle.text = @"Privacy Policy";
                return  cell;
            }
                break;
            case 1:
            {
                AppSettingDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AppSettingDetailCell" forIndexPath:indexPath];
                cell.lblTitle.text = @"Terms of Service";
                return  cell;
            }
                break;
                
            default:
                break;
        }
        

    }
    break ;
            //Log out
        case 2:
        {
            switch (indexPath.row) {
                case 0:
                {
                    AppSettingLogOutCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AppSettingLogOutCell" forIndexPath:indexPath];
                    cell.lblTitle.text = @"Log out";
                    return  cell;
                }
                    break;
               
                    
                default:
                    break;
            }
            
            
        }
        break ;
            case 3:
        {
            switch (indexPath.row) {
                case 0:
                {
                
                        AppSettingLogOutCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AppSettingLogOutCell" forIndexPath:indexPath];
                       cell.lblTitle.text = @"Deactivate Accout";
                    return  cell;
                    
                    break;
                }
                    break;
                    
                default:
                    break;
            }
        }

            break ;
        default:
            break;
    }

    return  0 ;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
            // Push ,new matches , upcoming , in App vibrations
            
        case 0:
        {
            switch (indexPath.row) {
                    
                case 0:
                {
                    
                    
                    
                }
                    break;
                case 1:
                {
                                   }
                    break;
                case 2:
                {
                  
                    
                    
                    
                    
                }
                    break;
                case 3:
                {
                    
                    
                    
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break ;
            // Privecy policy , terms of services
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    
                    CMSViewController *descovery = [DashStoryBoard instantiateViewControllerWithIdentifier:@"CMS"];
                    descovery.cmsId =1;
                    [self.navigationController pushViewController:descovery animated:YES ];
                }
                    break;
                case 1:
                {
                    CMSViewController *descovery = [DashStoryBoard instantiateViewControllerWithIdentifier:@"CMS"];
                    descovery.cmsId =2;
                    [self.navigationController pushViewController:descovery animated:YES ];
                }
                    break;
                    
                default:
                    break;
            }
            
            
        }
            break ;
            //Log out
        case 2:
        {
            switch (indexPath.row) {
                case 0:
                {
                    [self logOutAction ];
                }
                    break;
                    
                    
                default:
                    break;
            }
            
            
        }
            break ;
        case 3:
        {
            switch (indexPath.row) {
                case 0:
                {
                    [self deActivateAction];
                    
                    break;
                }
                    break;
                    
                default:
                    break;
            }
        }
            
            break ;
        default:
            break;
    }

}

#pragma Switch Action 
-(void)switchNotification: (UISwitch *)Sender
{
    
}
-(void)switchMatches: (UISwitch *)Sender
{
    
}
-(void)switchCalls: (UISwitch *)Sender
{
    
}
-(void)switchVibrate: (UISwitch *)Sender
{
    
}
#pragma mark Log out 
-(void)logOutAction
{
    [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,logoutAPI] withTag:logoutAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}
#pragma mark Log out
-(void)deActivateAction
{
    [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,deActivateAccountAPi] withTag:deActivateAccountAPiTag withParameter:dict];
    apiObject.delegate = self ;
    
}

#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    
    switch (tagCon) {
        case logoutAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                             [APPDELEGATE loginAsRoot];
                
                           }
            
            
            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            break ;
        case deActivateAccountAPiTag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                [APPDELEGATE loginAsRoot];
                
            }
            
            
            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            break ;
            
            
        default:
            break;
            
    }
    
}
- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Done

- (IBAction)doneAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
    
}
@end
