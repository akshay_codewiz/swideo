//
//  SettingViewController.h
//  Swideo
//
//  Created by akshay on 7/14/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXBlurView.h"
#import <QuartzCore/QuartzCore.h>
@interface SettingViewController : UIViewController
{
    __weak IBOutlet FXBlurView *blurView;
    __weak IBOutlet UITableView *tblView;
    __weak IBOutlet UIImageView *imgProfileTopBG;
    __weak IBOutlet UIImageView *imgProfile;
    NSMutableArray *arrData ;
}
- (IBAction)actionViewProfile:(id)sender;
- (IBAction)backAction:(id)sender;

@end
