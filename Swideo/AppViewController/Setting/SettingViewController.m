//
//  SettingViewController.m
//  Swideo
//
//  Created by akshay on 7/14/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "SettingViewController.h"
#import "SettingTableViewCell.h"
#import "AboubtMECell.h"
#import "AppSettingVC.h"
@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
    imgProfile.clipsToBounds = YES;    // Do any additional setup after loading the view.
//    tblView.estimatedRowHeight = 60.0 ;
//    tblView.rowHeight = UITableViewAutomaticDimension;

    [UIView animateWithDuration:0.5 animations:^{
        blurView.blurRadius = 2;
        blurView.tintColor = [UIColor whiteColor];
    }];

    
//
    [self getProfileAction];
    

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [imgProfile sd_setImageWithURL:[NSURL URLWithString:[[UserDefaultHelper sharedDefaults] getuserImage]]
                  placeholderImage:placeHolderImageUser
                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                         }];
    
    [imgProfileTopBG sd_setImageWithURL:[NSURL URLWithString:[[UserDefaultHelper sharedDefaults] getuserImage]]
                       placeholderImage:placeHolderImageUser
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                              }];
    
}
#pragma mark Log out
-(void)getProfileAction
{
    [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,getUserProfileAPI] withTag:getUserProfileAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}

#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    
    switch (tagCon) {
        case getUserProfileAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                arrData =[dictResponse valueForKey:@"data"];
                [tblView reloadData];
            }
            
            
            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            break ;
            
            
        default:
            break;
            
    }
    
}
- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4 ;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float fixheight = 60 ;
    
            switch (indexPath.row) {
                case 0:
                {
                    return  fixheight ;
                    
                }
                case 1:
                {
                    return  fixheight ;
                    
                }
                case 2:
                {
                    return  150 ;
                }
                case 3:
                    {
                        return  150 ;
                    }
                    
                        default:
                        return  fixheight ;
                        break ;
                    
                    
            }
        

    
}

#pragma mark
#pragma mark - TableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
        case 0:
        {
            SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCell" forIndexPath:indexPath];
            cell.lblTitle.text = @"Discovery Setting";
            cell.lblValue.text = @"Modify Distance, age and more";

            return  cell;


            
        }
            break;
        case 1:
        {
            SettingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCell" forIndexPath:indexPath];
            cell.lblTitle.text = @"App Setting";
            cell.lblValue.text = @"Notifications, account and more";
            return  cell;

        }
            break;
        case 2:
        {
            AboubtMECell *cell = [tableView dequeueReusableCellWithIdentifier:@"AboubtMECell" forIndexPath:indexPath];
            cell.lblHeader.text = @"About Me";
            cell.txtView.text = [arrData valueForKey:@"about"];
            cell.txtView.editable = NO ;
            cell.txtView.contentInset = UIEdgeInsetsMake(-5, 0, 0, 0); // Av
            return  cell;


        }
            break;
        case 3:
        {
            AboubtMECell *cell = [tableView dequeueReusableCellWithIdentifier:@"AboubtMECell" forIndexPath:indexPath];
            cell.lblHeader.text = @"Interests";
            cell.txtView.text = [arrData valueForKey:@"myInterest"];
            cell.txtView.editable = NO ;
            return  cell;


            
        }
            break;
            
        default:
            break;
    }
       return  0 ;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
           
            DiscoveryViewController *descovery = [ProfileStoryBoard instantiateViewControllerWithIdentifier:@"Discovery"];
            descovery.isEdit = true ;
            [self.navigationController pushViewController:descovery animated:YES ];

            
            
        }
            break;
        case 1:
        {
            AppSettingVC* home  =[DashStoryBoard instantiateViewControllerWithIdentifier:@"AppSettingVC"];
            [self.navigationController pushViewController:home animated:YES];

            
        }
            break;
        case 2:
        {
            [self actionViewProfile:nil];
            
        }
            break;
        case 3:
        {
                       
            [self actionViewProfile:nil];
            
        }
            break;
            
        default:
            break;
    }

 
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionViewProfile:(id)sender {
    MyProfileViewController * profile  =[ProfileStoryBoard instantiateViewControllerWithIdentifier:@"MyProfile"];
    profile.isEdit = true ;
    [self.navigationController pushViewController:profile animated:YES];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
@end
