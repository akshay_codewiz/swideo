//
//  AppSettingLogOutCell.h
//  Swideo
//
//  Created by akshay on 7/15/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppSettingLogOutCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
