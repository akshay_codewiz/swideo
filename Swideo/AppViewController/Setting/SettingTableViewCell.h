//
//  SettingTableViewCell.h
//  Swideo
//
//  Created by akshay on 7/14/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTableViewCell : UITableViewCell
{
    
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;

@end
