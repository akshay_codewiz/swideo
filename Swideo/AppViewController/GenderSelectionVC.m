//
//  GenderSelectionVC.m
//  Swideo
//
//  Created by akshay on 7/12/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "GenderSelectionVC.h"
#import "AgeCell.h"
@interface GenderSelectionVC ()

@end

@implementation GenderSelectionVC
@synthesize arrGenderSelected ;
- (void)viewDidLoad {
    [super viewDidLoad];
    if (_isEdit) {
        [self getGenderAPi];
    }
    
    // Do any additional setup after loading the view.
}
#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    
    switch (tagCon) {
        case getInterestgenderAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                
                arrGenderSelected = [NSMutableArray arrayWithArray:[dictResponse valueForKey:@"data"]];
                [_tblgender reloadData];
            }
            
            
            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            break ;
            case updateUserProfileAPITag:
        {
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                [self.navigationController popViewControllerAnimated:true];
            }
            
            
            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
            
        }
            break ;
            
        default:
            break;
            
    }
    
}
- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}

#pragma mark - Validate User name
-(void)getGenderAPi
{
    arrGenderSelected = [[NSMutableArray alloc]init];
//    if (!arrGenderSelected) {
//        arrGenderSelected =[[NSMutableArray alloc]init] ;
//    }
        [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[[UserDefaultHelper sharedDefaults ]getUserID] forKey:@"userId"];
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,getInterestgenderAPI] withTag:getInterestgenderAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
            return [arrGenderSelected count] ;
    
}

- (CGFloat)tableView:(UITableView *)tableView
estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
#pragma mark
#pragma mark - TableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
            AgeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Age" forIndexPath:indexPath];
            cell.lblGender.text =[[arrGenderSelected objectAtIndex:indexPath.row] valueForKey:@"name"];
             [cell.imgCheck setHidden:true];
    if ([[[arrGenderSelected objectAtIndex:indexPath.row] valueForKey:@"flag"] isEqualToString:@"1"]) {
        [cell.imgCheck setHidden:false];

    }
    
//    for (int i = 0;i <[arrGenderSelected count]; i++) {
//        NSString *strID =[[arrGender   valueForKey:@"genderId"] objectAtIndex:indexPath.row];
//        NSString *selectedID = [arrGenderSelected objectAtIndex:i];
//        if ([strID isEqualToString:selectedID]) {
//          [cell.imgCheck setHidden:false];
//        }
//        
//    }
            return  cell ;
            
    
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *isLike = [NSString stringWithFormat:@"%@",[[arrGenderSelected objectAtIndex:indexPath.row] valueForKey:@"flag"]];
    if ([isLike isEqualToString:@"0"])
    {
        isLike =@"1";
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
        [dict setValue:[[arrGenderSelected objectAtIndex:indexPath.row] valueForKey:@"name"] forKey:@"name"];
        [dict setValue:isLike forKey:@"flag"];
        [dict setValue:[[arrGenderSelected objectAtIndex:indexPath.row] valueForKey:@"genderId"] forKey:@"genderId"];
        
        [arrGenderSelected replaceObjectAtIndex:indexPath.row withObject:dict];
//        [arrGenderSelected addObject:[[arrGender objectAtIndex:indexPath.row] valueForKey:@"genderId"]];
    }
    else
    {
        isLike =@"0";
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
        [dict setValue:[[arrGenderSelected objectAtIndex:indexPath.row] valueForKey:@"name"] forKey:@"name"];
        [dict setValue:isLike forKey:@"flag"];
        [dict setValue:[[arrGenderSelected objectAtIndex:indexPath.row] valueForKey:@"genderId"] forKey:@"genderId"];
        
        [arrGenderSelected replaceObjectAtIndex:indexPath.row withObject:dict];
//        [arrGenderSelected removeObject:[[arrGender objectAtIndex:indexPath.row] valueForKey:@"genderId"]];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [_tblgender reloadData];
        
    });
    

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doneAction:(id)sender {
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"flag ==  %@", @"1"];
    if (_isEdit) {
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];

        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"flag ==  %@", @"1"];
        //                    arrSelectedCategory = [NSMutableArray arrayWithArray:[arrSelectedCategory filteredArrayUsingPredicate:predicate]];
        
        if ([NSMutableArray arrayWithArray:[arrGenderSelected filteredArrayUsingPredicate:predicate]].count > 0) {
            [ dict setValue:[GlobalMethods getJsonFromArray:[[NSMutableArray arrayWithArray:[arrGenderSelected filteredArrayUsingPredicate:predicate]] valueForKey:@"genderId"]] forKey:@"interestedGender"];
        }
        else
        {
//            NSArray *arr = [[NSArray alloc]init];
            [ dict setValue:@"[]" forKey:@"interestedGender"];

        }
//
        [dict setValue:[[UserDefaultHelper sharedDefaults]getUserID] forKey:@"userId"];

            CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,updateUserProfileAPI] withTag:updateUserProfileAPITag withParameter:dict];
            apiObject.delegate = self ;
            
    }
    else{
        
        
        [self.genderSelectiondelegate SelectGenderDelegate:arrGenderSelected ];
        [self.navigationController popViewControllerAnimated:true];
    }
    
}
@end
