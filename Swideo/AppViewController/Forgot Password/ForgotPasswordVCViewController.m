//
//  ForgotPasswordVCViewController.m
//  Swideo
//
//  Created by akshay on 7/18/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "ForgotPasswordVCViewController.h"

@interface ForgotPasswordVCViewController ()

@end

@implementation ForgotPasswordVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardWillShow :(NSNotification *)sender
{
    
    NSDictionary *info = [sender userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    btnTopConstant.constant = +height;
    [btnFP setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}
-(void)keyboardWillHide :(NSNotification *)sender
{
    NSDictionary *info = [sender userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    btnTopConstant.constant =  btnTopConstant.constant -  height;
    [btnFP setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Navigation

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 100;
    
}// return NO to not change text

// called when clear button pressed. return NO to ignore   (no notifications)
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return true;
}


#pragma mark
#pragma mark - Validate User name
-(void)FPAPIcall
{
    
            [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    // email,password,deviceId,deviceType,location,locationLatitude,locationLongitude,locationTimezone
    [dict setValue:txtForgotPWd.text forKey:@"email"];

    
    
    //
    
    //        NSLog(@"tzName-%@",tzName);
    //        BOOL isDayLightSavingTime = [[NSTimeZone timeZoneWithName:@"America/Anchorage"] isDaylightSavingTimeForDate:[NSDate date]];
    //        NSLog(@"isDayLightSavingTime-%d",isDayLightSavingTime);
    
    
    
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,forgotPasswordAPI] withTag:forgotPasswordAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}
#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    
    switch (tagCon) {
        case forgotPasswordAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                [self performSelector:@selector(backAction:) withObject:self afterDelay:0.6] ;
                
                            }
            
            
            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            break ;
            
        default:
            break;
            
    }
    
}
- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)forGotpwd:(id)sender {
    [self.view endEditing:true];
    
    if (txtForgotPWd.text.length <= 5) {
        [GlobalMethods showAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter a valid email"];
    }
    
    else
    {
        [self FPAPIcall];
    }
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
@end
