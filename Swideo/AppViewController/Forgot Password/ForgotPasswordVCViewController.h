//
//  ForgotPasswordVCViewController.h
//  Swideo
//
//  Created by akshay on 7/18/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordVCViewController : UIViewController
{
    __weak IBOutlet AkTextfield *txtForgotPWd;
    
    __weak IBOutlet UIButton *btnFP;
    __weak IBOutlet NSLayoutConstraint *btnTopConstant;
}
- (IBAction)forGotpwd:(id)sender;
- (IBAction)backAction:(id)sender;

@end
