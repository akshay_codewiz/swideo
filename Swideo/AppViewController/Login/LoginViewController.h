//
//  LoginViewController.h
//  Swideo
//
//  Created by akshay on 7/7/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AkTextfield.h"
@interface LoginViewController : UIViewController <UITextFieldDelegate>
{
    
    __weak IBOutlet AkTextfield *txtPwd;
    __weak IBOutlet AkTextfield *txtuserName;
    __weak IBOutlet NSLayoutConstraint *loginBtnBottom;
    __weak IBOutlet UIButton *btnLogin;
}
- (IBAction)backAction:(id)sender;
- (IBAction)actionForgotPwd:(id)sender;
- (IBAction)loginAction:(id)sender;

@end
