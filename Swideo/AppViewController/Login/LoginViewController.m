//
//  LoginViewController.m
//  Swideo
//
//  Created by akshay on 7/7/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "LoginViewController.h"
#import "EnhanceProfileVC.h"
#import "ForgotPasswordVCViewController.h"
#import "AccountConfirmationVC.h"
#import "FirstScreenViewController.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    // Do any additional setup after loading the view.
}
#pragma mark
#pragma mark - KeyBoard Hide Show Notification

-(void)keyboardWillShow :(NSNotification *)sender
{
    
    NSDictionary *info = [sender userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    loginBtnBottom.constant = +height;
    NSLog(@"%f",loginBtnBottom.constant);
    [btnLogin setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}
-(void)keyboardWillHide :(NSNotification *)sender
{
    NSDictionary *info = [sender userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    loginBtnBottom.constant =  loginBtnBottom.constant -  height;
    NSLog(@"%f",loginBtnBottom.constant);
    [btnLogin setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Navigation

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (textField == txtPwd) {
        return newLength <= 15;

    }
    return newLength <= 100;

}// return NO to not change text

// called when clear button pressed. return NO to ignore   (no notifications)
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return true;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    UIViewController *previousViewController = [[[self navigationController]viewControllers] objectAtIndex:([[[self navigationController]viewControllers] indexOfObject:self]-1)];
    if ([previousViewController isKindOfClass:[AccountConfirmationVC class]]) {
        FirstScreenViewController *first =[MainStoryBoard instantiateViewControllerWithIdentifier:@"FirstScreenViewController"];
        [self.navigationController pushViewController:first animated:false];
    }
    else{
        [self.navigationController popViewControllerAnimated:true];

    }

}

- (IBAction)actionForgotPwd:(id)sender {
//    ForgotPasswordVCViewController *FP = [MainStoryBoard instantiateViewControllerWithIdentifier:@"ForgotPassword"];
//    [self.navigationController pushViewController:FP animated:YES ];

}

- (IBAction)loginAction:(id)sender {
    
    
    [self.view endEditing:true];
    
    if (txtuserName.text.length <= 5) {
        [GlobalMethods showAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter a valid email or username"];
    }
   else if (txtPwd.text.length <= 5) {
        [GlobalMethods showAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter password of minimum 5 character"];
    }
    else
    {
        [self loginAPIcall];
    }

}
#pragma mark
#pragma mark - Validate User name
-(void)loginAPIcall
{
    
        [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
   // email,password,deviceId,deviceType,location,locationLatitude,locationLongitude,locationTimezone
    [dict setValue:txtuserName.text forKey:@"email"];
    [dict setValue:[GlobalMethods encodeStringTo64:txtPwd.text] forKey:@"password"];
    [dict setValue:[[UserDefaultHelper sharedDefaults]getDeviceToken] forKey:@"deviceId"];
    [dict setValue:@"2" forKey:@"deviceType"];
    [dict setValue:@"" forKey:@"locationLatitude"];
    [dict setValue:@"" forKey:@"locationLongitude"];
    
            NSTimeZone *timeZone = [NSTimeZone localTimeZone];
            NSString *tzName = [timeZone name];
           [dict setValue:tzName forKey:@"locationTimezone"];
//

//        NSLog(@"tzName-%@",tzName);
//        BOOL isDayLightSavingTime = [[NSTimeZone timeZoneWithName:@"America/Anchorage"] isDaylightSavingTimeForDate:[NSDate date]];
//        NSLog(@"isDayLightSavingTime-%d",isDayLightSavingTime);



    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,loginAPI] withTag:LoginAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}
#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    
    switch (tagCon) {
        case LoginAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                

               if([[[dictResponse  valueForKey:@"data"]valueForKey:@"isProfileActive"] boolValue] == true)
              {
                  [[UserDefaultHelper sharedDefaults]setUserName:[[dictResponse valueForKey:@"data"] valueForKey:@"userName"]];
                  [[UserDefaultHelper sharedDefaults]setFirstName:[[dictResponse valueForKey:@"data"] valueForKey:@"firstName"]];
                  [[UserDefaultHelper sharedDefaults]setLastName:[[dictResponse valueForKey:@"data"] valueForKey:@"lastName"]];
                  [[UserDefaultHelper sharedDefaults]setUserID:[[dictResponse valueForKey:@"data"] valueForKey:@"userId"]];
                  [[UserDefaultHelper sharedDefaults]setUserImage:[[dictResponse valueForKey:@"data"] valueForKey:@"profileImage"]];
                  [[UserDefaultHelper sharedDefaults]setEmail:[[dictResponse valueForKey:@"data"] valueForKey:@"email"]];
                  [[UserDefaultHelper sharedDefaults]ProfileCompleted:[[dictResponse valueForKey:@"data"] valueForKey:@"isProfileComplete"]] ;

                  
                  if ([[[dictResponse valueForKey:@"data"] valueForKey:@"isProfileComplete"] boolValue]) {
                      HomeViewController* home  =[DashStoryBoard instantiateViewControllerWithIdentifier:@"Home"];
                      [self.navigationController pushViewController:home animated:YES];
                  }
                  else{
                      EnhanceProfileVC *profile = [MainStoryBoard instantiateViewControllerWithIdentifier:@"EnhanceProfile"];
                      [self.navigationController pushViewController:profile animated:YES ];

                  }

                 

              }
              else{
                [self goToVarification];
              }
            }
            
            
            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];

            }
        }
            break ;
            
        default:
            break;
            
    }
    
}
- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}

-(void)goToVarification
{
    AccountConfirmationVC *login = [MainStoryBoard instantiateViewControllerWithIdentifier:@"AccountConfirmation"];
    [self.navigationController pushViewController:login animated:true];
}
@end
