//
//  CMSViewController.h
//  Swideo
//
//  Created by akshay on 7/20/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CMSViewController : UIViewController
{
    
    __weak IBOutlet UIWebView *webView;
    __weak IBOutlet UILabel *lblHeader;
}
@property(readwrite,nonatomic) int cmsId;
- (IBAction)backAction:(id)sender;

@end
