//
//  CMSViewController.m
//  Swideo
//
//  Created by akshay on 7/20/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "CMSViewController.h"

@interface CMSViewController ()

@end

@implementation CMSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    switch (self.cmsId) {
        case 1:
            lblHeader.text = @"Privacy Policy";
            break;
        case 2:
            lblHeader.text = @"Terms of Service";
            break;

            
        default:
            break;
    }
    [self getCMSAction];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma markCMS
-(void)getCMSAction
{
    [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[NSString stringWithFormat:@"%d",self.cmsId] forKey:@"cmsId"];
    
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,getCMSAPI] withTag:getCMSAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}
#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    
    switch (tagCon) {
        case getCMSAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
//                NSLog(@"%@",[[[dictResponse valueForKey:@"data"] valueForKey:@"cmsContent"] objectAtIndex:0]);
                NSURL *cmsUrl = [NSURL URLWithString:[[[dictResponse valueForKey:@"data"] valueForKey:@"cmsLink"] objectAtIndex:0]];
                [webView loadRequest:[NSURLRequest requestWithURL:cmsUrl]];
                [webView reload];
                
            }
            
            
            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            break ;
            
        default:
            break;
            
    }
    
}
- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
@end
