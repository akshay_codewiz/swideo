//
//  AgeGroupCell.h
//  Swideo
//
//  Created by akshay on 7/12/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgeGroupCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAgeGroup;
@property (weak, nonatomic) IBOutlet UISlider *sliderLbl;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
