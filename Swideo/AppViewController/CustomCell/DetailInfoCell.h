//
//  DetailInfoCell.h
//  Swideo
//
//  Created by akshay on 7/12/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;

@end
