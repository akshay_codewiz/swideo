//
//  DiscoveryViewController.m
//  Swideo
//
//  Created by akshay on 7/12/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "DiscoveryViewController.h"
#import "AgeCell.h"
#import "DetailInfoCell.h"
#import "AgeGroupCell.h"
#import "GenderSelectionVC.h"
#import "MyProfileViewController.h"
#import "SelectContactTimeVC.h"
@interface DiscoveryViewController ()

@end

@implementation DiscoveryViewController
@synthesize isEdit ;
- (void)viewDidLoad {
    [super viewDidLoad];
    //    _tblDescovery.estimatedRowHeight = 60.0 ;
    //    _tblDescovery.rowHeight = UITableViewAutomaticDimension;
    
    APPDELEGATE.UserSelectionDict = [[NSMutableDictionary alloc] init];
    maxDistence =  @"0" ;
    if (isEdit ) {
        [btnNext setTitle:@"Done" forState:UIControlStateNormal] ;
    }
    else
    {
        [self getGenderAPi];
        [self getAgeRange];
    }
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if (isEdit) {
        [self getProfileAction];
        
    }
}
#pragma mark Log out
-(void)getProfileAction
{
    [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,getUserProfileAPI] withTag:getUserProfileAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}

#pragma mark -
-(void)getGenderAPi
{
    arrGender = [[NSMutableArray alloc]init];
    [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    if (isEdit) {
        [dict setValue:[[UserDefaultHelper sharedDefaults]getUserID] forKey:@"userId"];
    }
    // email,password,deviceId,deviceType,location,locationLatitude,locationLongitude,locationTimezone
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,getGenderAPI] withTag:getGenderAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}
-(void)getAgeRange
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,getInterestAgrGroupAPI] withTag:getInterestAgrGroupAPItag withParameter:dict];
    apiObject.delegate = self ;
}

#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    
    switch (tagCon) {
        case getGenderAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                
                arrGender = [NSMutableArray arrayWithArray:[dictResponse valueForKey:@"data"]];
                arrInterested  = [NSMutableArray arrayWithArray:[dictResponse valueForKey:@"data"]];

         
                [_tblDescovery reloadData];
            }
            
            
            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            break ;
        case getInterestAgrGroupAPItag:
        {
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            arrInterestedAgeGroup = [dictResponse valueForKey:@"data"];
            selectedAgeIndex = 0 ;
            
        }
            break ;
        case getUserProfileAPITag:
        {
            // For Edti profile =---
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            [self setUpProfileWithDict:[dictResponse valueForKey:@"data"]];
            
            
            
        }
            break ;
        case updateUserProfileAPITag:
        {
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                [GlobalMethods showAlertViewToView:self andTitle:APPNAME andMessage:[dictResponse valueForKey:@"message"]];
                [self performSelector:@selector(backAction) withObject:self afterDelay:0.5];
            }
            else{
                
            }
            
        }
            break ;
            
        default:
            break;
            
    }
    
}
-(void)setUpProfileWithDict : (NSMutableDictionary *)detailDict
{
    arrGender = [NSMutableArray arrayWithArray:[detailDict valueForKey:@"genderName"]];
    for (int i = 0 ; i < arrGender.count; i ++) {
        if ([[NSString stringWithFormat:@"%@",[[arrGender objectAtIndex:i] valueForKey:@"flag"]] isEqualToString:@"1" ]) {
            selectedIndex =[NSIndexPath indexPathForRow:i inSection:0];
            [ APPDELEGATE.UserSelectionDict setValue:[[arrGender objectAtIndex:i]valueForKey:@"genderId"] forKey:@"genderId"];
            
            break ;
        }
    }
    arrInterestedAgeGroup = [[NSMutableArray alloc]init];
    arrInterestedAgeGroup  = [NSMutableArray arrayWithArray:[detailDict valueForKey:@"interestedAgeGroup"]];
    
    for (int i = 0 ; i < arrInterestedAgeGroup.count; i ++) {
        if ([[NSString stringWithFormat:@"%@",[[arrInterestedAgeGroup objectAtIndex:i] valueForKey:@"flag"]] isEqualToString:@"1" ]) {
            selectedAgeIndex = i ;
            
            NSIndexPath *indexPathAge= [NSIndexPath indexPathForRow:1 inSection:1];
            AgeGroupCell *cellAge = (AgeGroupCell *)[_tblDescovery cellForRowAtIndexPath:indexPathAge];
            cellAge.sliderLbl.value =[[[arrInterestedAgeGroup objectAtIndex:i] valueForKey:@"interestedAgeGroupId"] floatValue];
            

            break ;
        }
    }
    
    strPlaceName = [detailDict valueForKey:@"location"] ;
    [APPDELEGATE.UserSelectionDict setValue:[detailDict valueForKey:@"locationLatitude"] forKey:@"locationLatitude"];
    [APPDELEGATE.UserSelectionDict setValue:[detailDict valueForKey:@"locationLongitude"] forKey:@"locationLongitude"];
    [APPDELEGATE.UserSelectionDict setValue:[detailDict valueForKey:@"location"] forKey:@"location"];
    
    strWeekDayTime =[detailDict valueForKey:@"contactWeekday"];
    strWeekendTime =[detailDict valueForKey:@"contactWeekend"];
    strInterestedGender =[detailDict valueForKey:@"interestedGender"];
   maxDistence = [NSString stringWithFormat:@"%@",[detailDict valueForKey:@"maxDistance"]] ;

    [_tblDescovery reloadData];
    
    
    
}
- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark - TableView DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50 ;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    NSArray * arrSection =[[NSArray alloc]initWithObjects:@"I am a",@"Interested In",@"Located near",@"When to Contact", nil];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 55)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, tableView.frame.size.width, 40)];
    [label setFont:[UIFont boldSystemFontOfSize:14]];
    NSString *string =[arrSection objectAtIndex:section];
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:APPGRAYCOLOR]; //your background color...
    
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:view.bounds];
    view.layer.masksToBounds = NO;
    view.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    view.layer.shadowOffset = CGSizeMake(0.0f, 0.2f);
    view.layer.shadowOpacity = 0.8f;
    view.layer.shadowPath = shadowPath.CGPath;
    
    return view;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
        {
            return [arrGender count] ;
        }
            break;
        case 1:
        {
            return 2 ;
        }
            break;
        case 2:
        {
            return 2 ;
        }
            break;
        case 3:
        {
            return 2 ;
        }
            break;
            
            
        default:
            return 0 ;
            break ;
            
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4 ;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float fixheight = 60 ;
    switch (indexPath.section) {
        case 0:
        {
            return  fixheight ;
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    return  fixheight ;
                    
                }
                case 1:
                {
                    return  100 ;
                    
                }
                    
                    
            }
        }
            break;
        case 2:
        {
            switch (indexPath.row) {
                case 0:
                {
                    return  fixheight ;
                    
                }
                case 1:
                {
                    return  100 ;
                    
                }
                    
                    
            }
        }
            break;
            
        default:
            return fixheight ;
            break;
    }
    return 44 ;
    
}
#pragma mark
#pragma mark - TableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            
            AgeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Age" forIndexPath:indexPath];
            cell.lblGender.text =[[arrGender objectAtIndex:indexPath.row] valueForKey:@"name"];
            if ([indexPath isEqual:selectedIndex]) {
                [cell.imgCheck setHidden:false] ;
            } else {
                [cell.imgCheck setHidden:true] ;
            }
            return  cell ;
            
        }
            break;
            
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    DetailInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Detail" forIndexPath:indexPath];
                    cell.lblTitle.text = @"Gender";
                    cell.lblValue.text = @"";
                    if (isEdit) {
                        cell.lblValue.text =strInterestedGender ;
                    }
                    else{
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"flag ==  %@", @"1"];
                        if ([NSMutableArray arrayWithArray:[arrInterested filteredArrayUsingPredicate:predicate]].count > 0) {
                            
                            cell.lblValue.text  = [[[NSMutableArray arrayWithArray:[arrInterested filteredArrayUsingPredicate:predicate]] valueForKey:@"name"]  componentsJoinedByString:@","];

                        }

                    }
                    
                    
                    return cell;
                }
                    break ;
                case 1:
                {
                    AgeGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AgeGroup" forIndexPath:indexPath];
                    cell.sliderLbl.minimumValue = 0 ;
                    cell.sliderLbl.maximumValue = [arrInterestedAgeGroup count] -1 ;
                    [cell.sliderLbl addTarget:self action:@selector(updateAgeGroup :) forControlEvents:UIControlEventValueChanged];
                    if (arrInterestedAgeGroup.count > 0) {
                        cell.lblAgeGroup.text =[[arrInterestedAgeGroup objectAtIndex:selectedAgeIndex] valueForKey:@"groupRange"];
                    }
                    
                    
                    return cell ;
                }
                    
                    break;
                    
                default:
                    break;
            }
            return  0 ;
            
        }
            break;
            
        case 2:
        {
            
            switch (indexPath.row) {
                case 0:
                {
                    DetailInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Detail" forIndexPath:indexPath];
                    cell.lblTitle.text = @"Location";
                    if (strPlaceName.length <=0) {
                        cell.lblValue.text = @"Choose Location";
                    }
                    else{
                        cell.lblValue.text = strPlaceName;
                    }
                    
                    
                    return cell;
                }
                    break ;
                case 1:
                {
                    AgeGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AgeGroup" forIndexPath:indexPath];
                    cell.lblTitle.text = @"Maximum distance";
                    cell.sliderLbl.minimumValue = 0 ;
                    cell.sliderLbl.maximumValue =  150 /5;
                    [cell.sliderLbl addTarget:self action:@selector(updateDistence :) forControlEvents:UIControlEventValueChanged];
                    cell.lblAgeGroup.text =[NSString stringWithFormat:@"%@ mi.",maxDistence];
                    cell.sliderLbl.value =  [maxDistence floatValue]/5.0 ;
                    return cell ;
                }
                    
                    break;
                    
                default:
                    break;
            }
            
            
        }
            break;
        case 3:
        {
            switch (indexPath.row) {
                case 0:
                {
                    DetailInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Detail" forIndexPath:indexPath];
                    cell.lblTitle.text = @"Weekday";
                    cell.lblValue.text = @"";
                    if (isEdit) {
                        cell.lblValue.text = strWeekDayTime;
                    }
                    else{
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isWeekday ==  %@", @"1"];
                        //                    arrSelectedCategory = [NSMutableArray arrayWithArray:[arrSelectedCategory filteredArrayUsingPredicate:predicate]];
                        
                        if ([NSMutableArray arrayWithArray:[arrTimeRange filteredArrayUsingPredicate:predicate]].count > 0) {
//                            cell.lblValue.text = [[[arrTimeRange filteredArrayUsingPredicate:predicate] objectAtIndex:0] valueForKey:@"name"];
                            cell.lblValue.text  = [[[NSMutableArray arrayWithArray:[arrTimeRange filteredArrayUsingPredicate:predicate]] valueForKey:@"name"]  componentsJoinedByString:@","];

                        }
                    }
                    
                    
                    return cell;
                }
                    break ;
                case 1:
                {
                    DetailInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Detail" forIndexPath:indexPath];
                    cell.lblTitle.text = @"Weekend";
                    cell.lblValue.text = @"";
                    
                    if (isEdit) {
                        cell.lblValue.text = strWeekendTime;
                    }
                    else{
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isWeekend ==  %@", @"1"];
                        //                    arrSelectedCategory = [NSMutableArray arrayWithArray:[arrSelectedCategory filteredArrayUsingPredicate:predicate]];
                        
                        if ([NSMutableArray arrayWithArray:[arrTimeRange filteredArrayUsingPredicate:predicate]].count > 0) {
//                            cell.lblValue.text = [[[arrTimeRange filteredArrayUsingPredicate:predicate] objectAtIndex:0] valueForKey:@"name"];
                            cell.lblValue.text  = [[[NSMutableArray arrayWithArray:[arrTimeRange filteredArrayUsingPredicate:predicate]] valueForKey:@"name"]  componentsJoinedByString:@","];

                        }
                    }
                    
                    
                    return cell;
                }
                    break ;
                    
                    
                default:
                    break;
            }
        }
            break;
            
            
        default:
            break;
    }
    return  0 ;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            if (selectedIndex) {
                NSMutableDictionary *dict =[[NSMutableDictionary alloc] init];
                [dict setValue:[[arrGender objectAtIndex:selectedIndex.row]valueForKey:@"name"] forKey:@"name"];
                [dict setValue:[[arrGender objectAtIndex:selectedIndex.row]valueForKey:@"genderId"] forKey:@"genderId"];
                [dict setValue:@"0" forKey:@"flag"];
                [arrGender replaceObjectAtIndex:selectedIndex.row withObject:dict];
            }
            selectedIndex  = indexPath ;
            
            NSMutableDictionary *dict =[[NSMutableDictionary alloc] init];
            [dict setValue:[[arrGender objectAtIndex:indexPath.row]valueForKey:@"name"] forKey:@"name"];
            [dict setValue:[[arrGender objectAtIndex:indexPath.row]valueForKey:@"genderId"] forKey:@"genderId"];
            
            [dict setValue:@"1" forKey:@"flag"];
            [arrGender replaceObjectAtIndex:indexPath.row withObject:dict];
            [_tblDescovery reloadData];
            [ APPDELEGATE.UserSelectionDict setValue:[[arrGender objectAtIndex:indexPath.row]valueForKey:@"genderId"] forKey:@"genderId"];
            
        }
            break;
            
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                    
                {
                    GenderSelectionVC *GenderSelection = [ProfileStoryBoard instantiateViewControllerWithIdentifier:@"GenderSelection"];
                    GenderSelection.isEdit = isEdit ;
                    if (!isEdit) {
                        GenderSelection.arrGenderSelected =arrInterested ;
                    }
                    
                    GenderSelection.genderSelectiondelegate =self ;
                    [self.navigationController pushViewController:GenderSelection animated:YES ];
                    
                }
                    break;
                case 1:
                    
                {
                    
                }
                    break;
                    
                    
                default:
                    break;
            }
        }
            break ;
        case 2:
        {
            switch (indexPath.row) {
                case 0:
                {
                    EditLocationViewController *search =(EditLocationViewController *)[ProfileStoryBoard instantiateViewControllerWithIdentifier:@"EditLocationViewController"];
                    search.isUserLoction  = true ;
                    search.delegate = self ;
                    [self.navigationController pushViewController:search animated:YES ];
                }
                    break;
                case 1:
                {
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break ;
        case 3:
        {
            switch (indexPath.row) {
                case 0:
                {
                    SelectContactTimeVC *search =(SelectContactTimeVC *)[ProfileStoryBoard instantiateViewControllerWithIdentifier:@"SelectContactTime"];
                    search.timeSelectiondelegate = self ;
                    search.isEdit = isEdit ;
                    if (!isEdit) {
                        if (arrTimeRange.count >0) {
                            search.arrTimeSelected = arrTimeRange ;
                        }
                        
                    }
                    search.isWeekDay = true;
                    [self.navigationController pushViewController:search animated:YES ];
                }
                    break;
                case 1:
                {
                    SelectContactTimeVC *search =(SelectContactTimeVC *)[ProfileStoryBoard instantiateViewControllerWithIdentifier:@"SelectContactTime"];
                    search.isEdit = isEdit ;
                    search.timeSelectiondelegate = self ;
                    if (!isEdit) {
                        if (arrTimeRange.count >0) {
                            search.arrTimeSelected = arrTimeRange ;
                        }
                    }
                    
                    search.isWeekDay = false;
                    [self.navigationController pushViewController:search animated:YES ];
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break ;
            
        default:
            break;
    }
    
}
#pragma mark GenderSelection Delegate-
-(void)SelectGenderDelegate :(NSMutableArray *)arrSelectedGender  {
    arrInterested = arrSelectedGender ;
    [_tblDescovery reloadData];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"flag ==  %@", @"1"];
    //                    arrSelectedCategory = [NSMutableArray arrayWithArray:[arrSelectedCategory filteredArrayUsingPredicate:predicate]];
    
    if ([NSMutableArray arrayWithArray:[arrInterested filteredArrayUsingPredicate:predicate]].count > 0) {
        [ APPDELEGATE.UserSelectionDict setValue:[GlobalMethods getJsonFromArray:[[NSMutableArray arrayWithArray:[arrInterested filteredArrayUsingPredicate:predicate]] valueForKey:@"genderId"]] forKey:@"interestedGender"];
    }
    
}
#pragma mark update age Slider
-(void)updateAgeGroup :(UISlider *)slider
{
    selectedAgeIndex = slider.value ;
    [_tblDescovery reloadData];
    
}
-(void)updateDistence:(UISlider *)slider
{
    [_tblDescovery reloadData];
//    slider.value = (int)slider.value * 5 ;
    maxDistence = [NSString stringWithFormat:@"%d",(int)slider.value * 5] ;
    
}
-(void)selectLocationWithlattitude :(NSString *)Lattiude  andLongtute :(NSString *)longitude andPlaceName :(NSString *)placeName
{
    strPlaceName = placeName ;
    [_tblDescovery reloadData];
    [APPDELEGATE.UserSelectionDict setValue:Lattiude forKey:@"locationLatitude"];
    [APPDELEGATE.UserSelectionDict setValue:longitude forKey:@"locationLongitude"];
    [APPDELEGATE.UserSelectionDict setValue:placeName forKey:@"location"];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)actionnext:(id)sender {
    
    if (isEdit ) {
        [APPDELEGATE.UserSelectionDict setValue:[[arrInterestedAgeGroup objectAtIndex:selectedAgeIndex] valueForKey:@"interestedAgeGroupId"] forKey:@"interestedAgeGroupId"];
        [self setProfileAPI];
    }
    else{
        
        [APPDELEGATE.UserSelectionDict setValue:[[arrInterestedAgeGroup objectAtIndex:selectedAgeIndex] valueForKey:@"interestedAgeGroup"] forKey:@"interestedAgeGroupId"];
        [APPDELEGATE.UserSelectionDict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
        [APPDELEGATE.UserSelectionDict setValue:maxDistence  forKey:@"maxDistance"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"flag ==  %@", @"1"];
        NSPredicate *predicateweekend = [NSPredicate predicateWithFormat:@"isWeekend ==  %@", @"1"];
        NSPredicate *predicateweekday = [NSPredicate predicateWithFormat:@"isWeekday ==  %@", @"1"];
        //                    arrSelectedCategory = [NSMutableArray arrayWithArray:[arrSelectedCategory filteredArrayUsingPredicate:predicate]];
        
        
        if (!selectedIndex) {
            [GlobalMethods showAlertViewToView:self andTitle:APPNAME andMessage:@"Please choose gender"];
        }
        //                    arrSelectedCategory = [NSMutableArray arrayWithArray:[arrSelectedCategory filteredArrayUsingPredicate:predicate]];
        else if ([NSMutableArray arrayWithArray:[arrInterested filteredArrayUsingPredicate:predicate]].count <= 0)     {
            [GlobalMethods showAlertViewToView:self andTitle:APPNAME andMessage:@"Please choose you Interest"];
        }
        
        else if([NSMutableArray arrayWithArray:[arrTimeRange filteredArrayUsingPredicate:predicateweekend]].count <= 0 && [NSMutableArray arrayWithArray:[arrTimeRange filteredArrayUsingPredicate:predicateweekday]].count <= 0)
        {
            [GlobalMethods showAlertViewToView:self andTitle:APPNAME andMessage:@"Please select when to contact"];
            
        }
        else{
            
            if (strPlaceName.length <=0) {
                
                [APPDELEGATE.UserSelectionDict setValue:@"" forKey:@"locationLatitude"];
                [APPDELEGATE.UserSelectionDict setValue:@"" forKey:@"locationLongitude"];
                [APPDELEGATE.UserSelectionDict setValue:@"" forKey:@"location"];
                
                // [GlobalMethods showAlertViewToView:self andTitle:APPNAME andMessage:@"Please choose location"];
            }
            MyProfileViewController * profile  =[ProfileStoryBoard instantiateViewControllerWithIdentifier:@"MyProfile"];
            [self.navigationController pushViewController:profile animated:YES];
        }
    }
    
}
-(void)SelectContactTImeDelegate :(NSMutableArray *)arrSelectedTime  {
    arrTimeRange = arrSelectedTime ;
    [_tblDescovery reloadData];
    NSPredicate *predicateweekend = [NSPredicate predicateWithFormat:@"isWeekend ==  %@", @"1"];
    NSPredicate *predicateweekday = [NSPredicate predicateWithFormat:@"isWeekday ==  %@", @"1"];
    
    if ([NSMutableArray arrayWithArray:[arrTimeRange filteredArrayUsingPredicate:predicateweekend]].count > 0) {
        [ APPDELEGATE.UserSelectionDict setValue:[GlobalMethods getJsonFromArray:[[NSMutableArray arrayWithArray:[arrTimeRange filteredArrayUsingPredicate:predicateweekend]] valueForKey:@"timerangeId"]] forKey:@"Weekend"];
    }
    if ([NSMutableArray arrayWithArray:[arrTimeRange filteredArrayUsingPredicate:predicateweekday]].count > 0) {
        [ APPDELEGATE.UserSelectionDict setValue:[GlobalMethods getJsonFromArray:[[NSMutableArray arrayWithArray:[arrTimeRange filteredArrayUsingPredicate:predicateweekday]] valueForKey:@"timerangeId"]] forKey:@"Weekday"];
    }
    
    
}
#pragma  mark set Edit Profile
-(void)setProfileAPI
{
    [GlobalMethods showLoadingViewOnView:self.view :hudMessage];

    [APPDELEGATE.UserSelectionDict setValue:maxDistence  forKey:@"maxDistance"];
    [APPDELEGATE.UserSelectionDict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,updateUserProfileAPI] withTag:updateUserProfileAPITag withParameter:APPDELEGATE.UserSelectionDict];
    apiObject.delegate = self ;
    
}
-(void)backAction
{
    [self.navigationController popViewControllerAnimated:true];
    
}

@end
