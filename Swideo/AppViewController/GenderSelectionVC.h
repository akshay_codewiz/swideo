//
//  GenderSelectionVC.h
//  Swideo
//
//  Created by akshay on 7/12/16.
//  Copyright © 2016 akshay. All rights reserved.
//
@protocol genderSelectionDelegate;

#import <UIKit/UIKit.h>

@interface GenderSelectionVC : UIViewController
{
//    NSMutableArray *arrGender;
}
@property (nonatomic)id genderSelectiondelegate ;
@property (nonatomic ,readwrite)BOOL isEdit ;

@property (weak, nonatomic) IBOutlet UITableView *tblgender;
@property (strong, nonatomic) NSMutableArray *arrGenderSelected ;
- (IBAction)doneAction:(id)sender;

@end
@protocol SelectGenderDelegate <NSObject>
@optional
//Delegate Methods
-(void)SelectGenderDelegate :(NSMutableArray *)arrSelectedGender  ;
@end