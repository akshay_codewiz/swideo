//
//  EditLocationViewController.h
//  PALS
//
//  Created by Mihin on 13/04/16.
//  Copyright © 2016 Quantum Technolabs. All rights reserved.
//

@protocol selectLocation <NSObject>
-(void)selectLocationWithlattitude :(NSString *)Lattiude  andLongtute :(NSString *)longitude andPlaceName :(NSString *)placeName ;
@end
#import <UIKit/UIKit.h>

@interface EditLocationViewController : UIViewController
{
    IBOutlet UITableView * tblGoogleSearch ;
    IBOutlet UISearchBar *searchTextField;

    __weak IBOutlet UILabel *lblCurrentLocation;
    __weak IBOutlet NSLayoutConstraint *lblLocationHeight;
    
    NSString *selectedLat,*selectedLong,*strPlaceName ;
    __weak IBOutlet UIView *viewLocation;
}
- (IBAction)switchuseLocation:(id)sender;
- (IBAction)switchLocation:(id)sender;
- (IBAction)doneAction:(id)sender;
@property(nonatomic ,readwrite) BOOL isUserLoction ;
- (IBAction)actionCancel:(id)sender;
@property (nonatomic ,strong) id delegate ;
@end
