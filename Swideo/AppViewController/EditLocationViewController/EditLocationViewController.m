//
//  EditLocationViewController.m
//  PALS
//
//  Created by Mihin on 13/04/16.
//  Copyright © 2016 Quantum Technolabs. All rights reserved.
//

#import "EditLocationViewController.h"
NSString *const apiKey = @"AIzaSyDO2ur1GOGgQXFJ9KzfaA0zt8X4sCAihQY";  // Drenthe
//NSString *const apiKey = @"AIzaSyBWJaOD74AMYEQbRk4Qgz3KzJuM1T3vDZs";


@interface EditLocationViewController ()
@property NSString *substring;
@property NSTimer *autoCompleteTimer;
@property NSMutableArray *localSearchQueries;
@property NSMutableArray *pastSearchResults;
@property NSMutableArray *pastSearchWords;
@end
typedef NS_ENUM(NSUInteger, TableViewSection){
    TableViewSectionMain,
    TableViewSectionCount
};


@implementation EditLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.localSearchQueries = [NSMutableArray array];
    self.pastSearchWords = [NSMutableArray array];
    self.pastSearchResults = [NSMutableArray array];
    tblGoogleSearch.separatorStyle=UITableViewCellSelectionStyleNone ;
    
    lblLocationHeight.constant = 0 ;
    tblGoogleSearch.hidden = false;
    searchTextField.hidden = false;
    viewLocation.hidden = true ;
    

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView  methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        switch (section) {
            case TableViewSectionMain:
                return self.localSearchQueries.count;
                break;
                
    }
    return  0 ;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //  Google search table view
    
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        //cell=nil;
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        cell.textLabel.hidden=FALSE ;
        switch (indexPath.section) {
            case TableViewSectionMain: {
                NSDictionary *searchResult = [self.localSearchQueries objectAtIndex:indexPath.row];
                cell.textLabel.text = [searchResult[@"terms"] objectAtIndex:0][@"value"];
                cell.detailTextLabel.text = searchResult[@"description"];
                cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:16.0];
                cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:13.0];
            }break;
                
            default:
                break;
        }
        
        
        return cell;
    
    // Add table view
    
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.backgroundColor=[UIColor lightGrayColor];
    //    tableView.alpha=0.9;
    cell.backgroundColor=[UIColor whiteColor];
    cell.contentView.backgroundColor=[UIColor clearColor];
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
@try {
        
    
    if (tableView==tblGoogleSearch) {
        
        switch (indexPath.section) {
            case TableViewSectionMain: {
                //this is where it broke
                NSDictionary *searchResult = [self.localSearchQueries objectAtIndex:indexPath.row];
                NSString *placeID = [searchResult objectForKey:@"place_id"];
                [searchTextField resignFirstResponder];
                [self retrieveJSONDetailsAbout:placeID withCompletion:^(NSArray *place) {
                    
                    NSLog(@"palce -- %@",place);
                    
                    //                self.selectedLocation.name = [place valueForKey:@"name"];
                    //                self.selectedLocation.address = [place valueForKey:@"formatted_address"];
                    NSString *latitude = [NSString stringWithFormat:@"%@,",[place valueForKey:@"geometry"][@"location"][@"lat"]];
                    NSString *longitude = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lng"]];
                    
                    int itemcount  = [searchResult[@"terms"]count];
                    
                    int cityCont = itemcount-3 ;
                    if (cityCont < 1)
                        cityCont = 0 ;
                    
                    int statecount = itemcount - 2 ;
                    if (statecount < 1)
                        statecount = 1 ;

                    
                    
                    NSString *city = [searchResult[@"terms"] objectAtIndex:cityCont][@"value"];
                    NSString *state = @"" ;
                    if (itemcount >=2) {
                        state = [searchResult[@"terms"] objectAtIndex:statecount][@"value"];
                    }

                    NSString *placename ;
                    if (state.length > 0) {
                       placename = [NSString stringWithFormat:@"%@, %@",city,state];//[NSString stringWithFormat:@"%@",[place valueForKey:@"name"]];

                    }
                    else{
                        placename = [NSString stringWithFormat:@"%@",city];
}
                   
                    if (self.isUserLoction) {
                        [self.delegate selectLocationWithlattitude:latitude andLongtute:longitude andPlaceName:placename ];
                    }
                    else{
                        
                        //[APPDELEGATE.UserSearchDict setValue:latitude forKey:@"lat"];
                        //[APPDELEGATE.UserSearchDict setValue:longitude forKey:@"long"];
                        //[APPDELEGATE.UserSearchDict setValue:placename forKey:@"placeName"];
                    }
                    [self.navigationController popViewControllerAnimated:YES];

                    
                    
                }];
            }break;
                
            default:
                break;
        }
        
        
        }
        
    
}
    
    
    
@catch (NSException *exception) {
    
         }
@finally {
         
       }
    
    
}

#pragma mark - Autocomplete SearchBar methods

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [self.autoCompleteTimer invalidate];
    [self searchAutocompleteLocationsWithSubstring:self.substring];
    [searchTextField resignFirstResponder];
    [tblGoogleSearch reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSString *searchWordProtection = [searchTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"Length: %lu",(unsigned long)searchWordProtection.length);
    
    if (searchWordProtection.length != 0) {
        tblGoogleSearch.hidden=FALSE;
        [self runScript];
        
    } else {
        
        NSLog(@"The searcTextField is empty.");
        [searchTextField resignFirstResponder];
        self.localSearchQueries = [[NSMutableArray alloc]init];
        [tblGoogleSearch reloadData];
        
        tblGoogleSearch.hidden=TRUE;
    }
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    self.substring = [NSString stringWithString:searchTextField.text];
    self.substring= [self.substring stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    self.substring = [self.substring stringByReplacingCharactersInRange:range withString:text];
    
    if ([self.substring hasPrefix:@"+"] && self.substring.length >1) {
        self.substring  = [self.substring substringFromIndex:1];
        NSLog(@"This string: %@ had a space at the begining.",self.substring);
    }
    
    else{
        
    }
    
    
    return YES;
}


-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchTextField.text = @"";
    [self.localSearchQueries removeAllObjects];
    [tblGoogleSearch reloadData];
    [searchTextField resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)runScript{
    
    [self.autoCompleteTimer invalidate];
    self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.65f
                                                              target:self
                                                            selector:@selector(searchAutocompleteLocationsWithSubstring:)
                                                            userInfo:nil
                                                             repeats:NO];
}


- (void)searchAutocompleteLocationsWithSubstring:(NSString *)substring
{
    @try {
        
    [self.localSearchQueries removeAllObjects];
    [tblGoogleSearch reloadData];
    
    if (![self.pastSearchWords containsObject:self.substring]) {
        [self.pastSearchWords addObject:self.substring];
        NSLog(@"Search: %lu",(unsigned long)self.pastSearchResults.count);
        [self retrieveGooglePlaceInformation:self.substring withCompletion:^(NSArray * results) {
            NSLog(@"%@",results);
            
            if (![results containsObject:@"API Error"]) {
                [self.localSearchQueries addObjectsFromArray:results];
                NSDictionary *searchResult = @{@"keyword":self.substring,@"results":results};
                [self.pastSearchResults addObject:searchResult];
                [tblGoogleSearch reloadData];
            }
            
        }];
        
    }else {
        
        for (NSDictionary *pastResult in self.pastSearchResults) {
            if([[pastResult objectForKey:@"keyword"] isEqualToString:self.substring]){
                [self.localSearchQueries addObjectsFromArray:[pastResult objectForKey:@"results"]];
                [tblGoogleSearch reloadData];
            }
        }
    }
        
    } @catch (NSException *exception) {
        
    } @finally {
        
}
}


#pragma mark - Google API Requests


-(void)retrieveGooglePlaceInformation:(NSString *)searchWord withCompletion:(void (^)(NSArray *))complete{
    
@try {
            NSString *searchWordProtection = [searchWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (searchWordProtection.length != 0) {
        
        //CLLocation *userLocation = self.locationManager.location;
        NSString *currentLatitude =APPDELEGATE.strLatitute;// @(userLocation.coordinate.latitude).stringValue;
        NSString *currentLongitude = APPDELEGATE.strLongitute;;//@(userLocation.coordinate.longitude).stringValue;
        
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=establishment|geocode&location=%@,%@&radius=500&language=en&key=%@",searchWord,currentLatitude,currentLongitude,apiKey];
        NSLog(@"AutoComplete URL: %@",urlString);
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSArray *results = [jSONresult valueForKey:@"predictions"];
            
            if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"] || [jSONresult[@"status"] containsString:@"error"]){
                if (!error){
                    NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                    NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                    complete(@[@"API Error", newError]);
                    return;
                }
                complete(@[@"Actual Error", error]);
                return;
            }else{
                complete(results);
            }
        }];
        
        [task resume];
    }
        
        
    }
@catch (NSException *exception) {
        
    }
@finally {
        
}

    
}

-(void)retrieveJSONDetailsAbout:(NSString *)place withCompletion:(void (^)(NSArray *))complete {\
    
    @try {
     
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=%@",place,apiKey];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        NSArray *results = [jSONresult valueForKey:@"result"];
        
        if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
            if (!error){
                NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                complete(@[@"API Error", newError]);
                return;
            }
            complete(@[@"Actual Error", error]);
            return;
        }else{
            complete(results);
        }
    }];
    
    [task resume];
        
        
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)switchuseLocation:(UISwitch *)sender {
    
    if (sender.on) {
        viewLocation.hidden = false ;
    }
    else{
        viewLocation.hidden = true ;
    }
}

- (IBAction)switchLocation:(UISwitch *)sender {
    
    [self.view endEditing:true];

    
    if(![CLLocationManager locationServicesEnabled] || ([CLLocationManager authorizationStatus]!=kCLAuthorizationStatusAuthorizedWhenInUse && [CLLocationManager authorizationStatus]!=kCLAuthorizationStatusAuthorizedAlways))
    {
        
        [GlobalMethods showAlertViewToView:self andTitle:@"Location Service Disabled" andMessage:@"To re-enable, please go to Settings and turn on Location Service for this app."];
        sender.on = FALSE ;
        
    }
    else{
        if (sender.on) {
//            [APPDELEGATE.UserSearchDict setValue:APPDELEGATE.strLatitute forKey:@"lat"];
//            [APPDELEGATE.UserSearchDict setValue:APPDELEGATE.strLongitute forKey:@"long"];
            
            lblLocationHeight.constant = 20 ;
            tblGoogleSearch.hidden = true;
            searchTextField.hidden = true;
            [self getAddressFromLatLon:APPDELEGATE.strLatitute andLongitute:APPDELEGATE.strLongitute];
                  }
        else{
            lblCurrentLocation.text = @"Choose location" ;
//            lblLocationHeight.constant = 0 ;
            tblGoogleSearch.hidden = false;
            searchTextField.hidden = false;

            
        }
        
    }

    
    
    
}

#pragma mark Get UserAddress
-(void) getAddressFromLatLon:(NSString *)strLatttude andLongitute:(NSString *)strLongitude
{
    CLLocation *bestLocation =[[CLLocation alloc]initWithLatitude:[strLatttude floatValue] longitude:[strLongitude floatValue]];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:bestLocation
                   completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error){
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         NSString *placename =[[NSString alloc]init];
         //         if (placemark.name) {
         
         NSLog(@"addressDictionary --%@",[placemark.addressDictionary valueForKey:@"FormattedAddressLines"]);
         NSLog(@"%@",placemark.addressDictionary);
         
         
         
         if (![placemark.addressDictionary valueForKey:@"City"] && ![placemark.addressDictionary valueForKey:@"State"]) {
             
             placename = [NSString stringWithFormat:@"%@",[placemark.addressDictionary valueForKey:@"Name"] ];
         }
         else{
           placename = [NSString stringWithFormat:@"%@,%@",[placemark.addressDictionary valueForKey:@"City"], [placemark.addressDictionary valueForKey:@"State"]];
         }
         
         selectedLat = APPDELEGATE.strLatitute;
         selectedLong = APPDELEGATE.strLongitute;
         strPlaceName = placename;

         lblCurrentLocation.text = placename ;
//         [APPDELEGATE.UserSearchDict setValue:placename forKey:@"placeName"];
         
     }];
    
}

- (IBAction)doneAction:(id)sender {
    
    
    
    if (strPlaceName.length >0) {
        [self.delegate selectLocationWithlattitude:selectedLat andLongtute:selectedLong andPlaceName:strPlaceName];
    }
    [self.navigationController popViewControllerAnimated:true];
    
}
@end
