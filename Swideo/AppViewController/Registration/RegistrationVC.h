//
//  RegistrationVC.h
//  Swideo
//
//  Created by akshay on 7/7/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationVC : UIViewController
{
    UIPickerView *pickerView ;
    UIToolbar *pickertoolbar ;
    __weak IBOutlet AkTextfield *txtEmail;
    __weak IBOutlet AkTextfield *txtPassword;
    __weak IBOutlet AkTextfield *txtAgeGroup;
    __weak IBOutlet NSLayoutConstraint *btnCountinutBottom;
    __weak IBOutlet UIButton *btnCountinue;
    __weak IBOutlet AkTextfield *txtFirstName;
    __weak IBOutlet AkTextfield *txtLastName;
    __weak IBOutlet UIImageView *imgCheck;
    BOOL isEmailFirstResponder ;
    BOOL isValidFomrAPI ;
    NSArray *arrAge ;
    __weak IBOutlet AkTextfield *txtMobile;
}
- (IBAction)backAction:(id)sender;
- (IBAction)ActionCountinue:(id)sender;

@end
