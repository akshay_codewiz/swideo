//
//  AccountConfirmationVC.h
//  Swideo
//
//  Created by akshay on 7/8/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountConfirmationVC : UIViewController
{
    
    __weak IBOutlet UILabel *lblText;
}
- (IBAction)actionResetEmail:(id)sender;
- (IBAction)actionLogin:(id)sender;
@end
