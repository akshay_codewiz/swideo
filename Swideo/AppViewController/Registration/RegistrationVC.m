//
//  RegistrationVC.m
//  Swideo
//
//  Created by akshay on 7/7/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "RegistrationVC.h"

@interface RegistrationVC ()

@end

@implementation RegistrationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    imgCheck.hidden = true ;
    [self setupAgePicker];
    
    
    UIToolbar * keyboardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    
    keyboardToolBar.barStyle = UIBarStyleDefault;
    [keyboardToolBar setItems: [NSArray arrayWithObjects:
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard)],
                                nil]];
    txtMobile.inputAccessoryView = keyboardToolBar;


    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.view endEditing:true];

}
-(void)getAgeRange
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:txtEmail.text forKey:@"email"];
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,getAgrGroupAPI] withTag:getAgrGroupAPITag withParameter:dict];
    apiObject.delegate = self ;
}
-(void)setupAgePicker
{
    //add date picker
    pickerView = [[UIPickerView alloc]init];
    pickerView.backgroundColor = [UIColor clearColor];
    pickerView.delegate = self;
    //add toolbar to future date picker
    pickertoolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    pickertoolbar.barStyle = UIBarStyleDefault;
    [pickerView sizeToFit];
    
    //Bar Button Setup
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonPressed:)];
    [barItems addObject:cancelBtn];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [barItems addObject:flexSpace];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonPressed:)];
    [barItems addObject:doneBtn];
    
    [pickertoolbar setItems:barItems animated:YES];
    
    [pickerView reloadAllComponents];
}

#pragma mark
#pragma mark - KeyBoard Hide Show Notification

-(void)keyboardWillShow :(NSNotification *)sender
{
    
    NSDictionary *info = [sender userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    btnCountinutBottom.constant = +height;
    [btnCountinue setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}
-(void)keyboardWillHide :(NSNotification *)sender
{
    NSDictionary *info = [sender userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    btnCountinutBottom.constant =  btnCountinutBottom.constant -  height;
    [btnCountinue setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Navigation

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (textField == txtPassword) {
        return newLength <= 15;
        
    }
    if (textField == txtMobile) {
        return newLength <= 15;
        
    }
    return newLength <= 100;
    
}// return NO to not change text
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField ==txtAgeGroup) {
        [txtAgeGroup setInputView:pickerView];
        [txtAgeGroup setInputAccessoryView:pickertoolbar];
        [pickerView setHidden:NO];
        if (arrAge.count <= 0) {
            [self getAgeRange];
        }
        else{
            [pickerView reloadAllComponents];
        }
        
        
        pickerView.tag = 1;
//        txtAgeGroup.text = @"" ;
    }
    else if (textField == txtEmail)
    {
        isValidFomrAPI = false ;
        imgCheck.hidden = true;
        isEmailFirstResponder = true ;
    }
    
   
}



// called when clear button pressed. return NO to ignore   (no notifications)
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
     if (textField ==txtEmail) {
     if (![GlobalMethods IsValidEmail:txtEmail.text]) {
        [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter a valid email address"];
         }
          else{
          }
        }

    [textField resignFirstResponder];
    return true;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField == txtEmail) {
        isEmailFirstResponder = false ;
    
      if (![GlobalMethods IsValidEmail:txtEmail.text]) {
        [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter a valid email address"];
      }
      else if (!isValidFomrAPI)
     {
        [self  ValidateEmailFromAPI];
        
        //[GlobalMethods showAlertViewToView:self andTitle:APPNAME andMessage:@"This email is already in use.Please try diffrent email address"];
     }
    }

    
}// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
-(void)resignKeyboard {
    
       [txtMobile resignFirstResponder];
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
#pragma mark Action Countinue
- (IBAction)ActionCountinue:(id)sender {
    
    
    if (txtFirstName.text.length <= 0) {
        [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter your first name"];
     }
    else  if (txtLastName.text.length <= 0) {
        [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter your last name"];
    }
    else if (txtMobile.text.length <= 0)
    {
        [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter your mobile number"];

    }
    else if (![GlobalMethods IsValidEmail:txtEmail.text]) {
        [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter a valid email address"];
    }
    else if (!isValidFomrAPI)
    {
        
        if ([txtEmail isFirstResponder]) {
                    [self  ValidateEmailFromAPI];

        }
        else{
            [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"This email is already in use.Please try diffrent email address"];

        }

    }
    else if (txtPassword.text.length <= 5) {
        [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter password of minimum 5 character"];
    }
    else if (txtAgeGroup.text.length <= 0) {
        [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter your Age range"];
    }
    // need age group here
    else
    {
        [[UserDefaultHelper sharedDefaults]setEmail:txtEmail.text];
        [[UserDefaultHelper sharedDefaults]setFirstName:txtFirstName.text];
        [[UserDefaultHelper sharedDefaults]setLastName:txtLastName.text];
        [[UserDefaultHelper sharedDefaults]setPassword:txtPassword.text];
        
        
        NSInteger row = [pickerView selectedRowInComponent:0];
        [[UserDefaultHelper sharedDefaults] setuserAgeRange:[[arrAge valueForKey:@"ageGroupId"]objectAtIndex:row]];
        
        UserNameVC *login = [MainStoryBoard instantiateViewControllerWithIdentifier:@"UserName"];
        [self.navigationController pushViewController:login animated:YES ];
    }
    


   
}
#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    [GlobalMethods removeLoadingViewfromView:self.view];

    switch (tagCon) {
        case validateAPITag:
        {
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                imgCheck.hidden = false ;
                isValidFomrAPI = true ;
                imgCheck.image =[UIImage imageNamed:@"check_arrow"];
               
                if (txtFirstName.text.length <= 0 || txtLastName.text.length <= 0 || txtMobile.text.length <= 0 || ![GlobalMethods IsValidEmail:txtEmail.text] || !isValidFomrAPI || txtPassword.text.length <= 5 || txtAgeGroup.text.length <= 0) {
                }
               
                
                else
                {
                    [[UserDefaultHelper sharedDefaults]setEmail:txtEmail.text];
                    [[UserDefaultHelper sharedDefaults]setFirstName:txtFirstName.text];
                    [[UserDefaultHelper sharedDefaults]setLastName:txtLastName.text];
                    [[UserDefaultHelper sharedDefaults]setPassword:txtPassword.text];
                    
                    
                    NSInteger row = [pickerView selectedRowInComponent:0];
                    [[UserDefaultHelper sharedDefaults] setuserAgeRange:[[arrAge valueForKey:@"ageGroupId"]objectAtIndex:row]];
                    UserNameVC *login = [MainStoryBoard instantiateViewControllerWithIdentifier:@"UserName"];
                    [self.navigationController pushViewController:login animated:YES ];
                }
            }
            else{
                isValidFomrAPI = false ;
                imgCheck.hidden = false ;
                imgCheck.image =[UIImage imageNamed:@"crossic"];
                [GlobalMethods showTopAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
        
            break;
        case getAgrGroupAPITag:
        {
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            arrAge = [dictResponse valueForKey:@"data"];
             [pickerView reloadAllComponents];
            
        }
            
            break;

            
        default:
            break;
    }
   
    
}

- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}
#pragma mark
#pragma mark - Picker Delegate
// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
   return  1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
   return  arrAge.count;
}
- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[arrAge valueForKey:@"groupRange"]objectAtIndex:row];
}
#pragma mark - Bar Button Clicked Event
-(void)doneButtonPressed:(id)sender
{
    [pickerView setHidden:YES];
    if (pickerView.tag==1) {
        [txtAgeGroup resignFirstResponder];
        NSInteger row = [pickerView selectedRowInComponent:0];
        txtAgeGroup.text = [[arrAge valueForKey:@"groupRange"]objectAtIndex:row];
    }
    
}
-(void)cancelButtonPressed:(id)sender
{
    [pickerView setHidden:YES];
    [txtAgeGroup resignFirstResponder];
    
    
}



-(void)ValidateEmailFromAPI
{
    
    [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:txtEmail.text forKey:@"email"];
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,chechUsernameAPI] withTag:validateAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}
@end
