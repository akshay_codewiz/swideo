//
//  AccountConfirmationVC.m
//  Swideo
//
//  Created by akshay on 7/8/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "AccountConfirmationVC.h"

@interface AccountConfirmationVC ()

@end

@implementation AccountConfirmationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setColoredText];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)actionResetEmail:(id)sender {
    [self sendConfirmMailAction];
}
-(void)setColoredText
{
    NSString *strFirst = @"Before you can start looking, we sent a link to confirm your account to";
    NSString *strSecond = [[UserDefaultHelper sharedDefaults] getEmail];
    
    NSString *strComplete = [NSString stringWithFormat:@"%@ %@",strFirst,strSecond];
    
    NSMutableAttributedString *attributedString =[[NSMutableAttributedString alloc] initWithString:strComplete];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor whiteColor]
                             range:[strComplete rangeOfString:strFirst]];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:APPBLUECOLOR
                             range:[strComplete rangeOfString:strSecond]];
    
        
    
    lblText.attributedText = attributedString;
}
-(IBAction)actionLogin:(id)sender
{
 
    NSLog(@"%@",[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count -1 ]);
    if ([[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count -2 ] isKindOfClass:[LoginViewController class]] ) {
        [self.navigationController popViewControllerAnimated:true];
    }
    else
    {    LoginViewController *login = [MainStoryBoard instantiateViewControllerWithIdentifier:@"Login"];
         [self.navigationController pushViewController:login animated:YES ];
    }

}
#pragma mark
#pragma mark - SendMail User name
-(void)sendConfirmMailAction
{
    
    [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,sendConfirmMail] withTag:sendMailAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}
#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    
    switch (tagCon) {
        case sendMailAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        
            break ;
            
        default:
            break;
            
    }
    
}
- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
