//
//  UserNameVC.h
//  Swideo
//
//  Created by akshay on 7/7/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserNameVC : UIViewController
{
    __weak IBOutlet AkTextfield *txtuserName;
    __weak IBOutlet NSLayoutConstraint *loginBtnBottom;
    __weak IBOutlet UIButton *btnLogin;

    __weak IBOutlet UIImageView *imgCheck;
    __weak IBOutlet UIView *availableView;
    __weak IBOutlet UIImageView *availableImage;
    __weak IBOutlet UILabel *availableLbl;
}
- (IBAction)backAction:(id)sender;
- (IBAction)actionLogin:(id)sender;

@end
