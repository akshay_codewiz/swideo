//
//  UserNameVC.m
//  Swideo
//
//  Created by akshay on 7/7/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "UserNameVC.h"

@interface UserNameVC ()

@end

@implementation UserNameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    availableView.hidden = true ;
    imgCheck.hidden = true ;

    
    // Do any additional setup after loading the view.
}
#pragma mark
#pragma mark - KeyBoard Hide Show Notification

-(void)keyboardWillShow :(NSNotification *)sender
{
    
    NSDictionary *info = [sender userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    loginBtnBottom.constant = +height;
    NSLog(@"%f",loginBtnBottom.constant);
    [btnLogin setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}
-(void)keyboardWillHide :(NSNotification *)sender
{
    NSDictionary *info = [sender userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    loginBtnBottom.constant =  loginBtnBottom.constant -  height;
    NSLog(@"%f",loginBtnBottom.constant);
    [btnLogin setNeedsUpdateConstraints];
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark - Validate User name
-(void)ValidateEmailFromAPI
{
    
    [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:txtuserName.text forKey:@"userName"];
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,chechUsernameAPI] withTag:validateAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}
#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    [GlobalMethods removeLoadingViewfromView:self.view];

    switch (tagCon) {
        case validateAPITag:
        {
    NSDictionary * dictResponse  = (NSDictionary *)response ;
    if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
        
        
        availableView.hidden = false ;
        availableImage.image =[UIImage imageNamed:@"username_bg"];
        availableLbl.text = @"your username is available";
        imgCheck.hidden = false ;
        [self performSelector:@selector(RegistrationAPI ) withObject:nil afterDelay:1.0];

    }
    else{
        
        availableView.hidden = false ;
        availableImage.image =[UIImage imageNamed:@"loginbtn"];
        availableLbl.text = [dictResponse valueForKey:@"message"];
       // [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
        
         }
        }
     break ;
        case RegistrationAPITag:
        {
            NSDictionary * dictResponse  = (NSDictionary *)response ;

            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                [self goToVarification];

            }
            else{
                 [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];

            }

        }
            break ;
            
        default:
            break;
    
    }
    
}

#pragma mark - Navigation

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    
    
    NSRange spaceRange = [string rangeOfString:@" "];
    if (spaceRange.location != NSNotFound)
    {
        return NO;
    } else {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 30;
    }
    
  
    
}// return NO to not change text
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    availableView.hidden = true ;
    imgCheck.hidden = true;
    return true ;
}
// called when clear button pressed. return NO to ignore   (no notifications)
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField ==txtuserName) {
        if (txtuserName.text.length < 5) {
            [GlobalMethods showAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter minimum 5 characters username"];
        }
        else{
            [self  ValidateEmailFromAPI];
        }
    }
    
    [textField resignFirstResponder];
    return true;
}

- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}
-(void)goToVarification
{
    AccountConfirmationVC *login = [MainStoryBoard instantiateViewControllerWithIdentifier:@"AccountConfirmation"];
    [self.navigationController pushViewController:login animated:YES ];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)actionLogin:(id)sender {
    
    [self.view endEditing:true];
    if (txtuserName.text.length <5) {
        [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:@"Please enter minimum 5 characters"];
    }
    else{
        [self ValidateEmailFromAPI];
        
    }
    
}

#pragma mark
#pragma mark - Validate User name
-(void)RegistrationAPI
{
    
        [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[txtuserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"userName"];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getFirstName] forKey:@"firstName"];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getLastName] forKey:@"lastName"];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getEmail] forKey:@"email"];
    [dict setValue:[GlobalMethods encodeStringTo64:[[UserDefaultHelper sharedDefaults] getPassword]] forKey:@"password"];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getuserAgeRange] forKey:@"ageGroupId"];
    [dict setValue:[[UserDefaultHelper sharedDefaults]getDeviceToken] forKey:@"deviceId"];
    [dict setValue:@"2" forKey:@"deviceType"];
    
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    [dict setValue:tzName forKey:@"locationTimezone"];



    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,registrationAPI] withTag:RegistrationAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}

@end
