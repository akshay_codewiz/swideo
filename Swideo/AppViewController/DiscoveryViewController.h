//
//  DiscoveryViewController.h
//  Swideo
//
//  Created by akshay on 7/12/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscoveryViewController : UIViewController
{
    NSMutableArray *arrGender ;
    NSMutableArray *arrInterestedAgeGroup ;

    NSIndexPath *selectedIndex ;
    NSMutableArray *arrInterested ;
//    NSArray *arrAgeRange ;
    int selectedAgeIndex ;
    NSString *strPlaceName ;
    NSString *maxDistence ;
    NSString *strWeekDayTime ,*strWeekendTime ,*strInterestedGender ;
    NSMutableArray *arrTimeRange ;


    __weak IBOutlet UIButton *btnNext;

}
@property (readwrite ,nonatomic)BOOL isEdit ;
@property (weak, nonatomic) IBOutlet UITableView *tblDescovery;
- (IBAction)actionnext:(id)sender;

@end
