//
//  HomeViewController.m
//  Swideo
//
//  Created by akshay on 7/14/16.
//  Copyright © 2016 akshay. All rights reserved.
//
#define ACTION_MARGIN 120 //%%% distance from center where the action applies. Higher = swipe further in order for the action to be called
#define SCALE_STRENGTH 4 //%%% how quickly the card shrinks. Higher = slower shrinking
#define SCALE_MAX .93 //%%% upper bar for how much the card shrinks. Higher = shrinks less
#define ROTATION_MAX 1 //%%% the maximum rotation allowed in radians.  Higher = card can keep rotating longer
#define ROTATION_STRENGTH 320 //%%% strength of rotation. Higher = weaker rotation
#define ROTATION_ANGLE M_PI/8 //%%% Higher = stronger rotation angle
#define TABLEHEIGHT [UIScreen mainScreen].bounds.size.height -60
#define frontTableTag 101
#define backTableTag 102
#import "HomeViewController.h"
#import "HomeTableViewCell.h"
#import "HomeInfoTableViewCell.h"
#import "AKCollectionViewCell.h"
static NSString * const kCellIdentifier = @"CellIdentifier";

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self animatorGesture];
    tblBack.tag = backTableTag ;
    tblFront.tag =  frontTableTag;
    currentViewindex  = 0 ;
    pageControll.transform = CGAffineTransformMakeRotation(M_PI_2);
    tblFront.decelerationRate = UIScrollViewDecelerationRateFast;
    pageControll.numberOfPages =  0 ;
    [viewMatch setHidden:true];


    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self GetUseresAPIcall];

}
#pragma mark - Validate User name
-(void)GetUseresAPIcall
{
    
    [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[[UserDefaultHelper sharedDefaults]getUserID] forKey:@"userId"];

    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,browseProfileAPI] withTag:browseProfileAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}
#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    
    switch (tagCon) {
        case browseProfileAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                
                NSLog(@"%@",dictResponse);
                arrUsersData = [dictResponse valueForKey:@"data"];
                [frontView setHidden:false];
                [backView setHidden:false];
                [tblFront reloadData];
                [tblBack reloadData];
                pageControll.numberOfPages =[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userImages"] count] +1;
                pageControll.currentPage = 0 ;
                frontView.hidden = false ;
                backView.hidden = false ;
                pageControll.hidden = false;
            
            }
            
            
            else{
                pageControll.hidden = true;
                frontView.hidden = true;
                backView.hidden = true ;
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            break ;
        case likeDislikeProfileAPITag :
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                
            }
            else{
            }

        }
            break ;
            
        default:
            break;
            
    }
    
}
- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}

#pragma mark - Animator Methods
// This Method will add Gesture to the View
-(void)animatorGesture
{
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [pan setMaximumNumberOfTouches:5];
    [pan setDelaysTouchesBegan:YES];
    [frontView addGestureRecognizer:pan];
    
    

    
    animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)settingAction:(id)sender {
    SettingViewController* home  =[DashStoryBoard instantiateViewControllerWithIdentifier:@"Setting"];
    [self.navigationController pushViewController:home animated:YES];
}
#pragma mark - TableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userImages"] count] + 1;
}
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([cell isKindOfClass:[HomeInfoTableViewCell class]]) {
//        HomeInfoTableViewCell *infoCell = (HomeInfoTableViewCell *)cell ;
//        [infoCell setCollectionViewDataSourceDelegate:self indexPath:nil];
//
//    }
////    NSInteger index = cell.collectionView.indexPath.row;
//
//    CGFloat horizontalOffset = [self.contentOffsetDictionary[[@(index) stringValue]] floatValue];
//    [cell.collectionView setContentOffset:CGPointMake(horizontalOffset, 0)];
//}

#pragma mark - TableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        NSLog(@"%@",arrUsersData);
    switch (tableView.tag) {
        case frontTableTag:
            switch (indexPath.row) {
                case 0:
                {
                    static NSString *MyIdentifier = @"HomeCell";
                    HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                    if (cell == nil)
                    {
                        cell = [[HomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                    }
                    [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:[[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userImages"] valueForKey:@"profileImage"] objectAtIndex:indexPath.row]]
                                  placeholderImage:placeHolderImageApp
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                         }];
                    return cell;
                }
                    break;
                case 1:
                {
                    

                    if ([[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userImages"] count] ==1) {
                        static NSString *MyIdentifier = @"infoCell";
                        HomeInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                        if (cell == nil)
                        {
                            cell = [[HomeInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                        }
                        cell.lblUserName.text = [[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userName"] ;
                        cell.lblAge.text = [[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"ageGroup"] ;
                        cell.lblDesc.text = [[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"about"];
                        
                        if ([[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"distance"] isEqualToString:@"NA"]) {
                            cell.lblDistance.text =@"sdasdas"; [NSString stringWithFormat:@"Distance :%@",[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"distance"]];
                        }
                        else
                        {
                            cell.lblDistance.text =  [NSString stringWithFormat:@"%@ miles away",[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"distance"]];
                        }
                        cell.collectionView.dataSource = self ;
                        cell.collectionView.delegate = self ;
                        [cell.collectionView reloadData];

                        
                       // [cell setarrUsersData:arrUsersData andCurrentIndex:currentViewindex andDelegate:self ];
                        return cell;
                    }
                    else{
                        static NSString *MyIdentifier = @"HomeCell";
                        HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                        if (cell == nil)
                        {
                            cell = [[HomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                        }
                        [cell.imgProfile sd_setImageWithURL:[[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userImages"] valueForKey:@"profileImage"] objectAtIndex:indexPath.row]
                                           placeholderImage:placeHolderImageApp
                                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                  }];
                        return cell;
                    }

                    }
                    break;
                case 2:
                {
                    

                    if ([[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userImages"] count] ==2) {
                        static NSString *MyIdentifier = @"infoCell";
                        HomeInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                        if (cell == nil)
                        {
                            cell = [[HomeInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                        }
                        cell.lblUserName.text = [[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userName"] ;
                        cell.lblAge.text = [[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"ageGroup"] ;
                        cell.lblDesc.text = [[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"about"];
                        
                        if ([[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"distance"] isEqualToString:@"NA"]) {
                            cell.lblDistance.text = [NSString stringWithFormat:@"Distance :%@",[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"distance"]];
                        }
                        else
                        {
                            cell.lblDistance.text =  [NSString stringWithFormat:@"%@ miles away",[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"distance"]];
                        }
                        cell.collectionView.dataSource = self ;
                        cell.collectionView.delegate = self ;
                        [cell.collectionView reloadData];

                        return cell;
                    }
                    else{
                        static NSString *MyIdentifier = @"HomeCell";
                        HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                        if (cell == nil)
                        {
                            cell = [[HomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                        }
                        [cell.imgProfile sd_setImageWithURL:[[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userImages"] valueForKey:@"profileImage"] objectAtIndex:indexPath.row]
                                           placeholderImage:placeHolderImageApp
                                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                  }];
                        return cell;
                    }
                    
                }
                    break;
                case 3:
                {

                    if ([[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userImages"] count] ==3) {
                        static NSString *MyIdentifier = @"infoCell";
                        HomeInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                        if (cell == nil)
                        {
                            cell = [[HomeInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                        }
                        cell.lblUserName.text = [[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userName"] ;
                        cell.lblAge.text = [[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"ageGroup"] ;
                        cell.lblDesc.text = [[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"about"];
                        
                        if ([[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"distance"] isEqualToString:@"NA"]) {
                            cell.lblDistance.text = [NSString stringWithFormat:@"Distance :%@",[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"distance"]];
                        }
                        else
                        {
                            cell.lblDistance.text =  [NSString stringWithFormat:@"%@ miles away",[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"distance"]];
                        }
                        cell.collectionView.dataSource = self ;
                        cell.collectionView.delegate = self ;
                        [cell.collectionView reloadData];

                        return cell;
                    }
                    else{
                        static NSString *MyIdentifier = @"HomeCell";
                        HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                        if (cell == nil)
                        {
                            cell = [[HomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                        }
                        [cell.imgProfile sd_setImageWithURL:[[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userImages"] valueForKey:@"profileImage"] objectAtIndex:indexPath.row]
                                           placeholderImage:placeHolderImageApp
                                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                  }];
                        return cell;
                    }
                    
                }
                    break;
                case 4:
                {
                    static NSString *MyIdentifier = @"infoCell";
                    HomeInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                    if (cell == nil)
                    {
                        cell = [[HomeInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                    }
                    
                    cell.lblUserName.text = [[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userName"] ;
                    cell.lblAge.text = [[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"ageGroup"] ;
                    cell.lblDesc.text = [[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"about"];
                    
                    if ([[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"distance"] isEqualToString:@"NA"]) {
                        cell.lblDistance.text = [NSString stringWithFormat:@"Distance :%@",[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"distance"]];
                    }
                    else
                    {
                        cell.lblDistance.text =  [NSString stringWithFormat:@"%@ miles away",[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"distance"]];
                    }
                   
                    cell.collectionView.dataSource = self ;
                    cell.collectionView.delegate = self ;
                    [cell.collectionView reloadData];

                    return cell;
                }
                    break;

            }

            
            break;
            
        case backTableTag:
            switch (indexPath.row) {
                case 0:
                {
                    static NSString *MyIdentifier = @"HomeCell";
                    HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                    if (cell == nil)
                    {
                        cell = [[HomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                    }
                    if (arrUsersData.count >currentViewindex+1 ) {
                        [cell.imgProfile sd_setImageWithURL:[NSURL URLWithString:[[[[arrUsersData  objectAtIndex:currentViewindex+1] valueForKey:@"userImages"] valueForKey:@"profileImage"] objectAtIndex:indexPath.row]]
                                           placeholderImage:placeHolderImageApp
                                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                  }];

                    }
                     return cell;
                    
                }
                    break;
                default:
                {
                    static NSString *MyIdentifier = @"HomeCell";
                    HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                    return cell;

                }
                    break;
               
            }
            
            
            
        
    }
       //return nil;
        
        
    } @catch (NSException *exception) {
        
        
        NSLog(@"%@",exception.description);
        
    } @finally {
        
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    NSIndexPath *firstVisibleIndexPath = [[tblFront indexPathsForVisibleRows] objectAtIndex:0];
    pageControll.currentPage = firstVisibleIndexPath.row;

//

//
////    if ([scrollView.superview isKindOfClass:[UITableView class]]) {
////        pageControll.currentPage = scrollView.contentOffset.y/TABLEHEIGHT ;
//    pageControll.currentPage =scrollView.contentOffset.y/[UIScreen mainScreen].bounds.size.height ;


//    }


}// any offset changes
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    if(decelerate) return;
    if ([scrollView.superview isKindOfClass:[tblFront class]] ) {
        [self scrollViewDidEndDecelerating:tblFront];

    }
}


- (void)scrollViewDidEndDecelerating:(UITableView *)tableView {
    
    if ([tableView isKindOfClass:[tblFront class]]) {
            [tableView scrollToRowAtIndexPath:[tableView indexPathForRowAtPoint: CGPointMake(tableView.contentOffset.x, tableView.contentOffset.y+[UIScreen mainScreen].bounds.size.height/2)] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//   if (indexPath.row == [[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userImages"] count]) {
//       tableView.estimatedRowHeight = TABLEHEIGHT;
//         return  UITableViewAutomaticDimension;
//    }
//    else{
           return TABLEHEIGHT;
//    }
    
}


#pragma mark - Handle Pan Gesture
- (void)handlePan:(UIPanGestureRecognizer *)gesture
{

    //%%% this extracts the coordinate data from your swipe movement. (i.e. How much did you move?)
    xFromCenter = [gesture translationInView:gesture.view].x; //%%% positive for right swipe, negative for left
    yFromCenter = [gesture translationInView:gesture.view].y; //%%% positive for up, negative for down
    
    //%%% checks what state the gesture is in. (are you just starting, letting go, or in the middle of a swipe?)
    switch (gesture.state)
    {
            //%%% just started swiping
        case UIGestureRecognizerStateBegan:
        {
            self.originalPoint = gesture.view.center;
            break;
        };
            //%%% in the middle of a swipe
        case UIGestureRecognizerStateChanged:
        {
            //%%% dictates rotation (see ROTATION_MAX and ROTATION_STRENGTH for details)
            CGFloat rotationStrength = MIN(xFromCenter / ROTATION_STRENGTH, ROTATION_MAX);
            
            //%%% degree change in radians
            CGFloat rotationAngel = (CGFloat) (ROTATION_ANGLE * rotationStrength);
            
            //%%% amount the height changes when you move the card up to a certain point
            CGFloat scale = MAX(1 - fabsf(rotationStrength) / SCALE_STRENGTH, SCALE_MAX);
            
            //%%% move the object's center by center + gesture coordinate
            gesture.view.center = CGPointMake(self.originalPoint.x + xFromCenter, self.originalPoint.y + yFromCenter);
            
            //%%% rotate by certain amount
            CGAffineTransform transform = CGAffineTransformMakeRotation(rotationAngel);
            
            //%%% scale by certain amount
            CGAffineTransform scaleTransform = CGAffineTransformScale(transform, scale, scale);
            
            //%%% apply transformations
            gesture.view.transform = scaleTransform;
            [self updateOverlay:xFromCenter];
            

            
            break;
        };
            //%%% let go of the card
        case UIGestureRecognizerStateEnded: {
            [self afterSwipeAction:gesture];
            break;
        };
        case UIGestureRecognizerStatePossible:break;
        case UIGestureRecognizerStateCancelled:break;
        case UIGestureRecognizerStateFailed:break;
    }
    
   }
- (CGFloat)angleOfView:(UIView *)view
{
    return atan2(view.transform.b, view.transform.a);
}
-(void)updateOverlay:(CGFloat)distance
{
    if (distance > 0)
    {
        imgYes.hidden = NO;
        imgNo.hidden = YES;
        //        imgYes.alpha = MIN(fabsf(distance)/100, 0.4);
        imgYes.alpha = 1.0;
        //        overlayView.mode = GGOverlayViewModeRight;
    } else {
        //        overlayView.mode = GGOverlayViewModeLeft;
        imgYes.hidden = YES;
        imgNo.hidden = NO;
        //        imgNo.alpha = MIN(fabsf(distance)/100, 0.4);
        imgNo.alpha = 1.0;
    }
    
    //    overlayView.alpha = MIN(fabsf(distance)/100, 0.4);
}
- (void)afterSwipeAction:(UIPanGestureRecognizer *)gesture
{
    if (xFromCenter > ACTION_MARGIN) {
        [self rightAction:gesture];
    } else if (xFromCenter < -ACTION_MARGIN) {
        [self leftAction:gesture];
        
    } else { //%%% resets the card
        [UIView animateWithDuration:0.3
                         animations:^{
                             gesture.view.center = self.originalPoint;
                             gesture.view.transform = CGAffineTransformMakeRotation(0);
                             imgYes.hidden=YES;
                             imgNo.hidden = YES;
                             
                         }];
    }
}

//%%% called when a swipe exceeds the ACTION_MARGIN to the right
-(void)rightAction:(UIPanGestureRecognizer *)gesture
{
    CGPoint finishPoint = CGPointMake(500, 2*yFromCenter +self.originalPoint.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         gesture.view.center = finishPoint;
                     }completion:^(BOOL complete){
                         
                         CGPoint velocity = [gesture velocityInView:gesture.view.superview];
                         UIDynamicItemBehavior *dynamic = [[UIDynamicItemBehavior alloc] initWithItems:@[gesture.view]];
                         [dynamic addLinearVelocity:velocity forItem:gesture.view];
                         //                     [dynamic addAngularVelocity:angularVelocity forItem:swipeView];
                         [dynamic setAngularResistance:1.25];
                         
                         // when the view no longer intersects with its superview, go ahead and remove it
                         dynamic.action = ^{
                             
                             [animator removeAllBehaviors];
                             
                             gesture.view.transform = CGAffineTransformMakeRotation(0);
                             gesture.view.frame = CGRectMake(0, backView.frame.origin.y, backView.frame.size.width, backView.frame.size.height);
                         };
                         [animator addBehavior:dynamic];
                         
                         // add a little gravity so it accelerates off the screen (in case user gesture was slow)
                         
                         UIGravityBehavior *gravity = [[UIGravityBehavior alloc] initWithItems:@[gesture.view]];
                         gravity.magnitude = 0.7;
                         [animator addBehavior:gravity];
                         [self likeUser];

                     }];
    [animator removeAllBehaviors];
    //    [delegate cardSwipedRight:self];
    
    NSLog(@"YES");
}

//%%% called when a swip exceeds the ACTION_MARGIN to the left
-(void)leftAction:(UIPanGestureRecognizer *)gesture
{
    CGPoint finishPoint = CGPointMake(-500, 2*yFromCenter +self.originalPoint.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         gesture.view.center = finishPoint;
                     }completion:^(BOOL complete){
                         CGPoint velocity = [gesture velocityInView:gesture.view.superview];
                         UIDynamicItemBehavior *dynamic = [[UIDynamicItemBehavior alloc] initWithItems:@[gesture.view]];
                         [dynamic addLinearVelocity:velocity forItem:gesture.view];
                         //                     [dynamic addAngularVelocity:angularVelocity forItem:swipeView];
                         [dynamic setAngularResistance:1.25];
                         
                         
                         // when the view no longer intersects with its superview, go ahead and remove it
                         dynamic.action = ^{
                             
                             [animator removeAllBehaviors];
                             gesture.view.transform = CGAffineTransformMakeRotation(0);
                             gesture.view.frame = CGRectMake(0, backView.frame.origin.y, backView.frame.size.width, backView.frame.size.height);
                             
                         };
                         [animator addBehavior:dynamic];
                         [self unlikeUser];
                         


                         // add a little gravity so it accelerates off the screen (in case user gesture was slow)
                         
                         UIGravityBehavior *gravity = [[UIGravityBehavior alloc] initWithItems:@[gesture.view]];
                         gravity.magnitude = 0.7;
                         [animator addBehavior:gravity];
                    
                         
                     }];
    [animator removeAllBehaviors];
    
    //    [delegate cardSwipedLeft:self];
    
    NSLog(@"NO");
}
#pragma mark Like Unlike Action
- (IBAction)btnLike:(id)sender {
    [self likeUser];
}
- (IBAction)btnDislike:(id)sender {
    [self unlikeUser];
}
-(void)likeUser
{
    [self setLikeStatus:@"1"];
    if ([[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"isLike"] boolValue]) {
        
        
        //dispatch_sync(dispatch_get_main_queue(), ^{
            [self loadLikeView];

       //});
    }
    [self loadNextView];

}
-(void)unlikeUser
{    [self setLikeStatus:@"0"];
    [self loadNextView];
}
-(void)loadNextView
{
    imgNo.hidden = YES;
    imgYes.hidden = YES;
    if (currentViewindex < arrUsersData.count-1 ) {
        currentViewindex = currentViewindex + 1;
        [tblFront scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        [tblFront reloadData];
        pageControll.numberOfPages =[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userImages"] count]+1;
        pageControll.currentPage = 0 ;
        if (currentViewindex  < [arrUsersData count]-1) {
            [tblBack reloadData];
        }
        else{
            [backView setHidden:true];
            }
    }
    else{
        [pageControll setHidden:true];
        [frontView setHidden:true];
        [backView setHidden:true];
        }
}
#pragma mark Like Unlike Action END

#pragma mark collectionView Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSLog(@"count ===%d",[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"interest"] count]);

    return  [[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"interest"] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    @try {
        NSLog(@"%d",currentViewindex);
        NSLog(@"count ===%d",[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"interest"] count]);

        if (indexPath.row < [[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"interest"] count]) {
            AKCollectionViewCell *cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"AKCollectionViewCell" forIndexPath:indexPath];
            
            [cell.btnNew setTitle:[[[[arrUsersData objectAtIndex:currentViewindex] valueForKey:@"interest" ]valueForKey:@"name"]objectAtIndex:indexPath.row] forState:UIControlStateNormal];
            if ([[[[[arrUsersData objectAtIndex:currentViewindex] valueForKey:@"interest" ]valueForKey:@"flag"]objectAtIndex:indexPath.row] boolValue]) {
                cell.btnNew.backgroundColor =APPGREENCOLOR;
                [cell.btnNew setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                cell.btnNew.layer.borderColor =[UIColor clearColor].CGColor;
            }
            else{
                cell.btnNew.backgroundColor =[UIColor clearColor];

                cell.btnNew.layer.borderColor =[UIColor lightGrayColor].CGColor;
                [cell.btnNew setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            }
            cell.btnNew.layer.cornerRadius =4.0;
            cell.btnNew.layer.borderWidth =1.0;
            cell.btnNew.layer.masksToBounds = true ;
            
            
            return cell ;

        }
        else{
            AKCollectionViewCell *cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"AKCollectionViewCell" forIndexPath:indexPath];
            return cell;

        }
       } @catch (NSException *exception) {
        NSLog(@"%@",exception.description);

        
    } @finally {
        
    }
}

#pragma mark - UICollectionViewDelegateLeftAlignedLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
    NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                     NSForegroundColorAttributeName: [UIColor blackColor]};
    NSString *text = [[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"interest"] objectAtIndex:indexPath.row] valueForKey:@"name"];
    const CGSize textSize = [text sizeWithAttributes: userAttributes];
    CGFloat randomWidth = textSize.width+40;
    return CGSizeMake(randomWidth, 22);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;

   // return section == 0 ? 15 : 5;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",indexPath.row);
    
}
-(void)setLikeStatus :(NSString *)isLike
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[[UserDefaultHelper sharedDefaults]getUserID] forKey:@"userId"];
    [dict setValue:isLike forKey:@"matchStatus"];
    [dict setValue:[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"matchUserId"] forKey:@"matchUserId"];

    
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,likeDislikeProfileAPI] withTag:likeDislikeProfileAPITag withParameter:dict];
    apiObject.delegate = self ;
   
}


#pragma mark Action On Matches
-(void)setMatchesImages
{
    imgmyProfile.layer.cornerRadius = imgmyProfile.frame.size.width / 2;
    imgmyProfile.clipsToBounds = YES;    // Do any additional setup after loading the view.
    [imgmyProfile sd_setImageWithURL:[NSURL URLWithString:[[UserDefaultHelper sharedDefaults] getuserImage]]
                  placeholderImage:placeHolderImageUser
                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                         }];
    
    
    imgmatchProfile.layer.cornerRadius = imgmatchProfile.frame.size.width / 2;
    imgmatchProfile.clipsToBounds = YES;    // Do any additional setup after loading the view.
    [imgmatchProfile sd_setImageWithURL:[NSURL URLWithString:[[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"userImages"] valueForKey:@"profileImage"] objectAtIndex:0]]
                    placeholderImage:placeHolderImageUser
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                           }];

}
- (IBAction)ActionReport:(id)sender {
}

- (IBAction)scheduleACall:(id)sender {
    [viewMatch setHidden:false];
}

- (IBAction)actionkeepLooking:(id)sender {
    [viewMatch setHidden:true];
}
-(void)loadLikeView
{
    [self setMatchesImages];
    [viewMatch setHidden:false];
}

@end
