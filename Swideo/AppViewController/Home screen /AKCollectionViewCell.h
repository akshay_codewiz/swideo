//
//  AKCollectionViewCell.h
//  Swideo
//
//  Created by akshay on 7/22/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AKCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *myLable;
//@property (weak, nonatomic) IBOutlet AkButton *btnInterest;
@property (weak, nonatomic) IBOutlet UIButton *btnNew;

@end
