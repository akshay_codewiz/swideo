//
//  HaomeTableViewCell.h
//  Swideo
//
//  Created by akshay on 7/21/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;

@end
