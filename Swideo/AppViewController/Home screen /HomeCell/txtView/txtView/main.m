//
//  main.m
//  txtView
//
//  Created by akshay on 7/26/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
