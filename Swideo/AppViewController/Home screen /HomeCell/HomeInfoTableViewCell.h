//
//  HomeInfoTableViewCell.h
//  Swideo
//
//  Created by akshay on 7/21/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKCollectionViewCell.h"
@interface HomeInfoTableViewCell : UITableViewCell <UICollectionViewDataSource ,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *dfgf;
@property (weak, nonatomic) IBOutlet UIScrollView *scrView;
//@property (nonatomic ,retain) NSMutableArray *arrUsersData;
//@property (readwrite ,nonatomic)int currentViewindex ;
//-(void)setarrUsersData:(NSArray *)arr andCurrentIndex : (int)currentIndex andDelegate: (id) delegate ;
//- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath ;

@end
