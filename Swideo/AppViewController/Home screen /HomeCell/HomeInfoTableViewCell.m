//
//  HomeInfoTableViewCell.m
//  Swideo
//
//  Created by akshay on 7/21/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "HomeInfoTableViewCell.h"

@implementation HomeInfoTableViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView reloadData];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//-(void)setarrUsersData:(NSArray *)arr andCurrentIndex : (int)currentIndex andDelegate: (id) delegate {
//    arrUsersData = arr ;
//    currentViewindex = currentIndex ;
//    self.collectionView.delegate = delegate;
//    self.collectionView.dataSource = delegate;
//    [self.collectionView reloadData];
//}
//#pragma mark collectionView Delegate
//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
//{
//    NSLog(@"count ===%d",[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"interest"] count]);
//    
//    return  [[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"interest"] count];
//}
//
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    @try {
//        NSLog(@"%d",currentViewindex);
//        NSLog(@"count ===%d",[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"interest"] count]);
//        
//        if (indexPath.row < [[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"interest"] count]) {
//            AKCollectionViewCell *cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"AKCollectionViewCell" forIndexPath:indexPath];
//            
//            [cell.btnNew setTitle:[[[[arrUsersData objectAtIndex:currentViewindex] valueForKey:@"interest" ]valueForKey:@"name"]objectAtIndex:indexPath.row] forState:UIControlStateNormal];
//            if ([[[[[arrUsersData objectAtIndex:currentViewindex] valueForKey:@"interest" ]valueForKey:@"flag"]objectAtIndex:indexPath.row] boolValue]) {
//                cell.btnNew.backgroundColor =APPGREENCOLOR;
//                [cell.btnNew setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//                cell.btnNew.layer.borderColor =[UIColor clearColor].CGColor;
//            }
//            else{
//                cell.btnNew.layer.borderColor =[UIColor lightGrayColor].CGColor;
//                [cell.btnNew setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//            }
//            cell.btnNew.layer.cornerRadius =3.0;
//            cell.btnNew.layer.borderWidth =1.0;
//            cell.btnNew.layer.masksToBounds = true ;
//            
//            
//            return cell ;
//            
//        }
//        else{
//            AKCollectionViewCell *cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"AKCollectionViewCell" forIndexPath:indexPath];
//            return cell;
//            
//        }
//    } @catch (NSException *exception) {
//        NSLog(@"%@",exception.description);
//        
//        
//    } @finally {
//        
//    }
//}

//#pragma mark - UICollectionViewDelegateLeftAlignedLayout
//
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
//    NSDictionary *userAttributes = @{NSFontAttributeName: font,
//                                     NSForegroundColorAttributeName: [UIColor blackColor]};
//    NSString *text = [[[[arrUsersData  objectAtIndex:currentViewindex] valueForKey:@"interest"] objectAtIndex:indexPath.row] valueForKey:@"name"];
//    const CGSize textSize = [text sizeWithAttributes: userAttributes];
//    CGFloat randomWidth = textSize.width+10;
//    return CGSizeMake(randomWidth, 20);
//}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
//{
//    return 0;
//    
//    //    return section == 0 ? 15 : 5;
//}
//
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(0, 0, 0, 0);
//}
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSLog(@"%d",indexPath.row);
//    
//}
//- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath
//{
//    self.collectionView.dataSource = dataSourceDelegate;
//    self.collectionView.delegate = dataSourceDelegate;
////    self.collectionView.indexPath = indexPath;
//    [self.collectionView setContentOffset:self.collectionView.contentOffset animated:NO];
//    
//    [self.collectionView reloadData];
//}


@end
