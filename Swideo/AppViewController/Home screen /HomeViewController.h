//
//  HomeViewController.h
//  Swideo
//
//  Created by akshay on 7/14/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>
{
    
    __weak IBOutlet UITableView *tblBack;
    __weak IBOutlet UITableView *tblFront;
    __weak IBOutlet UIView *backView;
    __weak IBOutlet UIView *frontView;
    IBOutlet UIImageView *imgYes;
    IBOutlet UIImageView *imgNo;
    NSArray * arrUsersData ;
    
    __weak IBOutlet UICollectionView *collectionViewInterest;
    int currentViewindex ;
    
    __weak IBOutlet UIPageControl *pageControll;
    
    UIDynamicAnimator *animator;
    CGFloat xFromCenter;
    CGFloat yFromCenter;

    __weak IBOutlet UILabel *lblNoData;
    __weak IBOutlet UIView *viewMatch;
    __weak IBOutlet UIImageView *imgmyProfile;
    __weak IBOutlet UIImageView *imgmatchProfile;
}
- (IBAction)settingAction:(id)sender;
@property (nonatomic)CGPoint originalPoint;
- (IBAction)btnLike:(id)sender;
- (IBAction)ActionReport:(id)sender;
- (IBAction)scheduleACall:(id)sender;
- (IBAction)actionkeepLooking:(id)sender;
- (IBAction)btnDislike:(id)sender;
@end
