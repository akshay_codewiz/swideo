//
//  SelectContactTimeVC.h
//  Swideo
//
//  Created by akshay on 7/14/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SelectionContactDelegate;
@interface SelectContactTimeVC : UIViewController
{
    __weak IBOutlet UITableView *tblView;
    
}
@property (nonatomic ,readwrite) BOOL isEdit ;
@property (nonatomic,readwrite)  BOOL isWeekDay ;

@property (nonatomic,retain)    NSMutableArray *arrTimeSelected ;

- (IBAction)doneAction:(id)sender;
@property (nonatomic)id timeSelectiondelegate ;

@end
@protocol SelectionContactDelegate <NSObject>
@optional
//Delegate Methods
-(void)SelectContactTImeDelegate :(NSMutableArray *)arrSelectedTime  ;
@end