//
//  FirstScreenViewController.h
//  Swideo
//
//  Created by akshay on 7/7/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstScreenViewController : UIViewController
{
    
    __weak IBOutlet UIScrollView *scrView;
    int currentIndex ;
    NSTimer *timer ;

}
- (IBAction)loginAction:(id)sender;
- (IBAction)signUpAction:(id)sender;

@end
