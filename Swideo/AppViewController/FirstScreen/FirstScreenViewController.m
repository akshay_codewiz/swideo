//
//  FirstScreenViewController.m
//  Swideo
//
//  Created by akshay on 7/7/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "FirstScreenViewController.h"
#define animationTime 3.0

@interface FirstScreenViewController ()

@end

@implementation FirstScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    scrView.contentInset = UIEdgeInsetsMake(0,0,0,0);
    [timer invalidate];
    timer=nil ;
    timer=[NSTimer scheduledTimerWithTimeInterval:animationTime target:self selector:@selector(scrollViewAnimation) userInfo:nil repeats:YES];/////akshay

    // Do any additional setup after loading the view.
}
#pragma mark Animation Timer
-(void)scrollViewAnimation
{
    if (currentIndex<2) {
        
        currentIndex =currentIndex+1 ;
        //if (scrollVw.contentOffset.y<MAX_ALLOWED_OFFSET) {
        //scroll to desire position
        [scrView setContentOffset:CGPointMake(currentIndex*scrView.frame.size.width, 0) animated:YES];
//        pageControl.currentPage=currentIndex;
        
    }
    else{
        currentIndex =0 ;
        //if (scrollVw.contentOffset.y<MAX_ALLOWED_OFFSET) {
        //scroll to desire position
        [scrView setContentOffset:CGPointMake(currentIndex*scrView.frame.size.width, 0) animated:NO];
//        pageControl.currentPage=currentIndex;
    }
    //}
    
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    
       if (timer) {
        [timer invalidate];
        timer=nil;
    }
    
}


-(void)viewWillAppear:(BOOL)animated
{
   // [self setUpScrollView];

}
#pragma mark set up scrollView
-(void)setUpScrollView
{
    CGFloat imgXOrigin = 0 ;
    for (int i =0 ; i < 3; i ++) {
        UIImageView *imgView  =[[UIImageView alloc] init];
        imgView.frame = CGRectMake(imgXOrigin, scrView.frame.origin.y, DEVICE_SIZE.width, scrView.frame.size.height);
        imgView.image =[UIImage imageNamed:@"bg"];
        
        [scrView addSubview:imgView];
        imgXOrigin = imgXOrigin + DEVICE_SIZE.width ;
    }
    [scrView setContentSize:CGSizeMake(imgXOrigin, scrView.frame.size.height)];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - loginSignUpAction

- (IBAction)loginAction:(id)sender {
    
    LoginViewController *login = [MainStoryBoard instantiateViewControllerWithIdentifier:@"Login"];
    [self.navigationController pushViewController:login animated:YES ];
}

- (IBAction)signUpAction:(id)sender {
    RegistrationVC *Registration = [MainStoryBoard instantiateViewControllerWithIdentifier:@"Registration"];
    [self.navigationController pushViewController:Registration animated:YES ];
}
@end
