//
//  MyProfileViewController.m
//  Swideo
//
//  Created by akshay on 7/13/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#define Maxrange 500
#import "MyProfileViewController.h"
#import "ProfileCell.h"
#import "InterestListVC.h"
#import "ProfileCompleteVC.h"
@interface MyProfileViewController ()

@end

@implementation MyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrImageID =[[NSMutableArray alloc]init] ;
    [arrImageID addObject:@""];
    [arrImageID addObject:@""];
    [arrImageID addObject:@""];
    [arrImageID addObject:@""];
    btnDeleteBigImage.hidden = true;
    btnDeletFirstImage.hidden = true;
    btnDeleteSecondImage.hidden = true;
    btnDeleteThirdImage.hidden = true;

    
   
    [self getProfileAPI];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    if (_isEdit) {
        [btnNext setTitle:@"Done" forState:UIControlStateNormal];
        [ self getProfileAction] ;
    }
}

#pragma  mark set user Profile
-(void)getProfileAPI
{
        [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
    [dict setValue:[[UserDefaultHelper sharedDefaults]getUserID] forKey:@"userId"];
    NSLog(@"%@",APPDELEGATE.UserSelectionDict);
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,getProfileImageAPI] withTag:getProfileImageAPITag withParameter:dict];
    apiObject.delegate = self ;
    
    
}
#pragma mark Log out
-(void)getProfileAction
{
        [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,getUserProfileAPI] withTag:getUserProfileAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}

#pragma mark
#pragma mark - API Delegate



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  2;
}
#pragma mark - TableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileCell" forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0:
            
        {
            cell.lblTitle.text = @"About Me";
           // cell.txtView.placeholder =@"" ;
            
            cell.txtView.delegate = self ;
            cell.txtView.editable = true  ;
            cell.txtView.userInteractionEnabled = true  ;
            if (_isEdit) {
                cell.txtView.text = [arrDAta valueForKey:@"about"];
                
            }
             cell.txtView.contentInset = UIEdgeInsetsMake(-5, 0, 0, 0); // Av
            
        }
            break;
        case 1:
            
        {
            cell.lblTitle.text = @"My Interests";
//            cell.txtView.editable = false  ;
            cell.txtView.delegate = self ;
            cell.txtView.userInteractionEnabled = true  ;
            cell.txtView.tag =1001 ;

            if (_isEdit) {
                cell.txtView.text = [arrDAta valueForKey:@"myInterest"];
               // cell.txtView.placeholder = @"Choose Interest";
                
            }
            else{
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"flag ==  %@", @"1"];
                cell.txtView.text = @"";
               
                if ([NSMutableArray arrayWithArray:[arrSelInterest filteredArrayUsingPredicate:predicate]].count > 0) {
                    
                    NSString * strInterst =@"" ;
                    for (int i =0 ; i < [NSMutableArray arrayWithArray:[arrSelInterest filteredArrayUsingPredicate:predicate]].count; i ++) {
//                        NSLog(@"%@",[[[arrSelInterest filteredArrayUsingPredicate:predicate] objectAtIndex:i] valueForKey:@"name"]) ;
                        strInterst =[strInterst stringByAppendingString:[NSString stringWithFormat:@"%@, ",[[[arrSelInterest filteredArrayUsingPredicate:predicate] objectAtIndex:i] valueForKey:@"name"]]];
                    }
                    if (strInterst > 0) {
                        cell.txtView.text = [strInterst substringToIndex:[strInterst length]-2]; // remove last comma
                    }
                    
                }
                else{
                    cell.txtView.placeholder = @"Choose Interest";
                    
                }
            }
            
        }
            break;
            
        default:
            break;
    }
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row == 0) {
    return 130;
//    }
//    else{
//        
//        return UITableViewAutomaticDimension ;
//    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            InterestListVC *interest = [ProfileStoryBoard instantiateViewControllerWithIdentifier:@"InterestList"];
            interest.isEdit = _isEdit ;
            if (_isEdit) {
                
            }
            else{
                if (arrSelInterest.count > 0) {
                    interest.arrInterest = arrSelInterest ;
                }
                
            }
            
            interest.InterestSelectiondelegate  = self ;
            [self.navigationController pushViewController:interest animated:YES];
            
            
            
        }
            
            break ;
        default:
            break;
    }
}
//- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
//{
//    NSLog(@"tap gesture");
////    CGPoint touchPoint=[gesture locationInView:scrollView];
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark action textView Delegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    UIToolbar * keyboardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    
    keyboardToolBar.barStyle = UIBarStyleDefault;
    [keyboardToolBar setItems: [NSArray arrayWithObjects:
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard)],
                                nil]];
    textView.inputAccessoryView = keyboardToolBar;
    
    
    
    CGPoint pointInTable = [textView.superview convertPoint:textView.frame.origin toView:tblView];
    CGPoint contentOffset = tblView.contentOffset;
    contentOffset.y = (pointInTable.y - textView.inputAccessoryView.frame.size.height);
    NSLog(@"contentOffset is: %@", NSStringFromCGPoint(contentOffset));
    [tblView setContentOffset:contentOffset animated:YES];
    
}
-(void)resignKeyboard {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    ProfileCell *cell1 = (ProfileCell *)[tblView cellForRowAtIndexPath:indexPath];
    [cell1.txtView resignFirstResponder];

    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    
//    if ([text isEqualToString:@"\n"]) {
//        
    
//        [textView resignFirstResponder];
    
        if ([textView.superview.superview isKindOfClass:[UITableViewCell class]])
        {
            UITableViewCell *cell = (UITableViewCell*)textView.superview.superview;
            NSIndexPath *indexPath = [tblView indexPathForCell:cell];
            [tblView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
        }
        
        return YES;
        
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
//    }
    
    // For any other character return TRUE so that the text gets added to the view
    return textView.text.length + (text.length - range.length) <= Maxrange;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (textView.tag == 1001) {
        
        CGPoint textViewPosition = [textView convertPoint:CGPointZero toView:tblView];
        NSIndexPath *indexPath = [tblView indexPathForRowAtPoint:textViewPosition];
        NSLog(@"%d",indexPath.row);
       [self tableView:tblView didSelectRowAtIndexPath:indexPath];
        return NO;

    }
    return YES;
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (_isEdit) {
        [self setAboutME];
    }
}
-(void)setAboutME {
    
    NSLog(@"%@",APPDELEGATE.UserSelectionDict);
    NSMutableDictionary * dict   = [[NSMutableDictionary alloc]init];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    ProfileCell *cell1 = (ProfileCell *)[tblView cellForRowAtIndexPath:indexPath];
    
   
    
    if (cell1.txtView.text.length == 0) {
        [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter something about you"];
    }
    else{
        [dict setValue:cell1.txtView.text forKey:@"about"];
        [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];

        CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,updateUserProfileAPI] withTag:updateAboutMETag withParameter:dict];
        apiObject.delegate = self ;
    }
    

    
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
    
}
#pragma mark action SelectInterestDelegate
-(void)SelectInterestDelegate :(NSMutableArray *)arrSelectedInterest
{
    NSLog(@"arrSelectedInterest --%@",arrSelectedInterest);
    arrSelInterest = arrSelectedInterest ;
    [tblView reloadData];
}

#pragma mark action Select Image

- (IBAction)actionSelectBigImage:(id)sender {
    selectedImgeTag = 0 ;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: @"Cancel"
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: @"Take a new photo", @"Choose from existing", nil];
    [actionSheet showInView:self.view];
}
- (IBAction)ActionSelectFirstImage:(id)sender {
    selectedImgeTag = 1 ;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: @"Cancel"
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: @"Take a new photo", @"Choose from existing", nil];
    [actionSheet showInView:self.view];
}
- (IBAction)actionSelectSecondImage:(id)sender {
    selectedImgeTag = 2 ;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: @"Cancel"
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: @"Take a new photo", @"Choose from existing", nil];
    [actionSheet showInView:self.view];
    
}
- (IBAction)actionSelectThirdImage:(id)sender {
    selectedImgeTag = 3 ;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: @"Cancel"
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: @"Take a new photo", @"Choose from existing", nil];
    [actionSheet showInView:self.view];
}
#pragma mark action Delete Image

- (IBAction)actionDeleteBigImage:(id)sender {
    selectedImgeTag  = 0 ;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:@"1" forKey:@"isProfile"];
    [dict setValue:[arrImageID objectAtIndex:selectedImgeTag] forKey:@"imageId"];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,removeProfileImageAPI] withTag:removeProfileImageAPITag withParameter:dict];
    
    apiObject.delegate = self ;
    imgBig.image = [UIImage imageNamed:@"camera-big"];
    btnDeleteBigImage.hidden = true ;

    
}


- (IBAction)actionDeleteFirstImage:(id)sender {
    
        [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    selectedImgeTag  = 1 ;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:@"0" forKey:@"isProfile"];
    [dict setValue:[arrImageID objectAtIndex:selectedImgeTag] forKey:@"imageId"];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,removeProfileImageAPI] withTag:removeProfileImageAPITag withParameter:dict];
    
    apiObject.delegate = self ;
    imgOne.image = [UIImage imageNamed:@"camera-big"];
    btnDeletFirstImage.hidden = true ;

    
    
}



- (IBAction)actionDeleteSecondImage:(id)sender {
    selectedImgeTag  = 2 ;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:@"0" forKey:@"isProfile"];
    [dict setValue:[arrImageID objectAtIndex:selectedImgeTag] forKey:@"imageId"];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,removeProfileImageAPI] withTag:removeProfileImageAPITag withParameter:dict];
    
    apiObject.delegate = self ;
    _imgTwo.image = [UIImage imageNamed:@"camera-big"];
    btnDeleteSecondImage.hidden = true ;

}


- (IBAction)actionDeleThirdImage:(id)sender {
    selectedImgeTag  = 3 ;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:@"0" forKey:@"isProfile"];
    [dict setValue:[arrImageID objectAtIndex:selectedImgeTag] forKey:@"imageId"];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,removeProfileImageAPI] withTag:removeProfileImageAPITag withParameter:dict];
    
    apiObject.delegate = self ;
    imgThree.image = [UIImage imageNamed:@"camera-big"];
    btnDeleteThirdImage.hidden = true ;

}
#pragma mark Done Action
- (IBAction)doneAction:(id)sender {
    // For Edit profile
    if (_isEdit) {
        // For set up profile from registration
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        ProfileCell *cell1 = (ProfileCell *)[tblView cellForRowAtIndexPath:indexPath];
        
        NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
        ProfileCell *cell2 = (ProfileCell *)[tblView cellForRowAtIndexPath:indexPath1];
        
        if ([cell1.txtView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ].length == 0) {
            [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter something about you"];
        }
        else if (cell2.txtView.text.length == 0){
            [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please Choose your interest"];
            
        }
        else if (cell2.txtView.text.length == 0){
            [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please Choose your interest"];
            
        }
        else if ([imgBig.image isEqual:[UIImage imageNamed:@"camera-big"]])
        {
            [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please select your profile image"];
            
        }
        else{
            [self.navigationController popViewControllerAnimated:true];
        }
    }
    else{
        // For set up profile from registration
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        ProfileCell *cell1 = (ProfileCell *)[tblView cellForRowAtIndexPath:indexPath];
        
        NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
        ProfileCell *cell2 = (ProfileCell *)[tblView cellForRowAtIndexPath:indexPath1];
        
        if ([cell1.txtView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ].length == 0) {
            [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please enter something about you"];
        }
        else if (cell2.txtView.text.length == 0){
            [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please Choose your interest"];
            
        }
        
        else if ([imgBig.image isEqual:[UIImage imageNamed:@"camera-big"]])
        {
            [GlobalMethods showTopAlertViewToView:self andTitle:APPNAME andMessage:@"Please select your profile image"];

        }
        else{
            
            [APPDELEGATE.UserSelectionDict setValue:cell1.txtView.text forKey:@"about"];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"flag ==  %@", @"1"];
            if ([NSMutableArray arrayWithArray:[arrSelInterest filteredArrayUsingPredicate:predicate]].count > 0) {
                
                [APPDELEGATE.UserSelectionDict setValue:[GlobalMethods getJsonFromArray:[[NSMutableArray arrayWithArray:[NSMutableArray arrayWithArray:[arrSelInterest filteredArrayUsingPredicate:predicate]]] valueForKey:@"interestId"]] forKey:@"myInterest"];
                
                [self setProfileAPI];
                
            }
            
            
            
        }
    }
}
#pragma mark actionactionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self takeNewPhotoFromCamera];
            break;
        case 1:
            [self choosePhotoFromExistingImages];
            break ;
        case 2:
            break ;
        default:
            break;
            
    }
    
    
}


- (void)takeNewPhotoFromCamera
{
    if  (   [GlobalMethods checkImagePermission])
    {
        
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            controller.allowsEditing = YES;
            controller.cameraCaptureMode =UIImagePickerControllerCameraCaptureModePhoto ;
            controller.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil] ;
            controller.delegate = self;
            [self presentViewController: controller animated: YES completion: nil];
        }
    }
    else{
        [APPDELEGATE showAlertViewForCamera];
    }
}

-(void)choosePhotoFromExistingImages
{
    
    
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    if (status == ALAuthorizationStatusDenied  || status == ALAuthorizationStatusRestricted) {
        [APPDELEGATE showAlertViewForPhotos];
        
        //show alert for asking the user to give permission
        
    }
    else if (status == ALAuthorizationStatusNotDetermined) {
        
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init] ;
        
        ALAssetsGroupEnumerationResultsBlock assetsEnumerator = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
            // handle asset
            NSLog(@"Success");
            
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            controller.allowsEditing = YES;
            controller.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil] ;
            controller.delegate = self;
            [self presentViewController: controller animated: YES completion: nil];
        };
        
        ALAssetsLibraryGroupsEnumerationResultsBlock groupsEnumerator = ^(ALAssetsGroup *group, BOOL *stop) {
            // handle group
            NSLog(@"Success");
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            controller.allowsEditing = YES;
            controller.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil] ;
            controller.delegate = self;
            [self presentViewController: controller animated: YES completion: nil];
            
        };
        
        ALAssetsLibraryAccessFailureBlock failHandler = ^(NSError *error) {
            NSLog(@"Failled");
            // handle error
        };
        
        
        [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:groupsEnumerator failureBlock:failHandler];
        //show alert for asking the user to give permission
        
    }
    
    else{
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            controller.allowsEditing = YES;
            controller.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil] ;
            controller.delegate = self;
            [self presentViewController: controller animated: YES completion: nil];
        }
    }
    
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.navigationController dismissViewControllerAnimated: YES completion:^{
        UIImage *image = [info valueForKey: UIImagePickerControllerEditedImage];
        switch (selectedImgeTag) {
            case 0:
            {
                imgBig.image = image ;
                [self updateProfilePic];
            }
                break;
            case 1:
                imgOne.image = image ;
                [self updateProfilePic];
                
                break;
            case 2:
                _imgTwo.image = image ;
                [self updateProfilePic];
                
                break;
            case 3:
                imgThree.image = image ;
                [self updateProfilePic];
                
                break;
                
            default:
                break;
        }
        
        
    }];
}
-(void)updateProfilePic
{
        [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    UIImage * imageToUpload  ;
    switch (selectedImgeTag) {
        case 0:
        {
            [dict setValue:@"1" forKey:@"isProfile"];
            [dict setValue:@"0" forKey:@"orderNo"];
            imageToUpload = imgBig.image ;
            btnDeleteBigImage.hidden = false;
            
        }
            break;
        case 1:
        {
            [dict setValue:@"0" forKey:@"isProfile"];
            [dict setValue:@"1" forKey:@"orderNo"];
            imageToUpload = imgOne.image ;
            btnDeletFirstImage.hidden = false;
            
        }
            break;
        case 2:
        {
            [dict setValue:@"0" forKey:@"isProfile"];
            [dict setValue:@"2" forKey:@"orderNo"];
            imageToUpload = _imgTwo.image ;
            btnDeleteSecondImage.hidden = false;
            
            
        }
            break;
        case 3:
        {
            [dict setValue:@"0" forKey:@"isProfile"];
            [dict setValue:@"3" forKey:@"orderNo"];
            imageToUpload = imgThree.image ;
            btnDeleteThirdImage.hidden = false;
            
            
        }
            break;
            
            
        default:
            break;
    }
    
    
    [dict setValue:[arrImageID objectAtIndex:selectedImgeTag] forKey:@"imageId"];
    [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,UserProfileImageAPI] withTag:UserProfileImageAPItag withParameter:dict ImageName:imageToUpload andImageKey:@"profileImage"];
    apiObject.delegate = self ;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
}
#pragma  mark set user Profile
-(void)setProfileAPI
{
        [GlobalMethods showLoadingViewOnView:self.view :hudMessage];
    NSLog(@"%@",APPDELEGATE.UserSelectionDict);
    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,updateUserProfileAPI] withTag:updateUserProfileAPITag withParameter:APPDELEGATE.UserSelectionDict];
    apiObject.delegate = self ;
    
}
#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    NSLog(@"%d",tagCon);
    [GlobalMethods removeLoadingViewfromView:self.view];

    switch (tagCon) {
        case UserProfileImageAPItag:
        {
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                [arrImageID replaceObjectAtIndex:selectedImgeTag withObject:[NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"data"] valueForKey:@"imageId"]]];
                NSLog(@"%@",arrImageID);
                
                
            }
            
            
            else{
                [GlobalMethods showTopAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            break ;
        case removeProfileImageAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                [arrImageID replaceObjectAtIndex:selectedImgeTag withObject:@"" ];
                
            }
            
            
            else{
                [GlobalMethods showTopAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
            
        }
            break ;
            
            
            
        case updateUserProfileAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                if ([[dictResponse valueForKey:@"isProfileComplete"] boolValue]) {
                    [[UserDefaultHelper sharedDefaults]ProfileCompleted:@"1"];
                    [[UserDefaultHelper sharedDefaults]setUserImage:[[dictResponse valueForKey:@"data"] valueForKey:@"profileImage"]];
                    
                    
                    ProfileCompleteVC * profile  =[ProfileStoryBoard instantiateViewControllerWithIdentifier:@"ProfileComplete"];
                    [self.navigationController pushViewController:profile animated:YES];
                    
                }
                [GlobalMethods showTopAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
            }
            
            
            else{
                [GlobalMethods showTopAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
            
        }
            break ;
        case getProfileImageAPITag:
        {
            
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                [self setUpImageWithArray:[[dictResponse valueForKey:@"data"] valueForKey:@"images"]];
                
            }
            else{
                [GlobalMethods showTopAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            break ;
        case getUserProfileAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                arrDAta = [dictResponse valueForKey:@"data"];
                NSLog(@"%@",arrDAta);
                [tblView reloadData];
                if (_isEdit) {
                    [[UserDefaultHelper sharedDefaults]setUserImage:[[dictResponse valueForKey:@"data"] valueForKey:@"profileImage"]];

                }
                
                
            }
            else{
                [GlobalMethods showTopAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
            
        }
            break ;

            case updateAboutMETag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                arrDAta = [dictResponse valueForKey:@"data"];
                [tblView reloadData];

                
            }
            else{
                [GlobalMethods showTopAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }

        }
            break ;

        default:
            break;
            
    }
    
}
#pragma mark setup users profile  Image
-(void)setUpImageWithArray : (NSArray *)arrofImages
{
    for (int i = 0 ; i < [arrofImages count]; i++) {
        
        
        switch ([[[arrofImages valueForKey:@"orderNo"] objectAtIndex:i] intValue]) {
            case 0:
            {
                
                if ([[[arrofImages valueForKey:@"profileImage"] objectAtIndex:i] length] > 0)
                {
                    [arrImageID replaceObjectAtIndex:0 withObject:[NSString stringWithFormat:@"%@",[[arrofImages valueForKey:@"imageId"] objectAtIndex:i]]];
                    [imgBig sd_setImageWithURL:[NSURL URLWithString:[[arrofImages valueForKey:@"profileImage"] objectAtIndex:i]]
                              placeholderImage:placeHolderImageUser
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                     }];
                    btnDeleteBigImage.hidden = false;
                }
                
                
                
            }
                break;
            case 1:
            {
                [arrImageID replaceObjectAtIndex:1 withObject:[NSString stringWithFormat:@"%@",[[arrofImages valueForKey:@"imageId"] objectAtIndex:i]]];
                [imgOne sd_setImageWithURL:[NSURL URLWithString:[[arrofImages valueForKey:@"profileImage"] objectAtIndex:i]]
                          placeholderImage:placeHolderImageUser
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                 }];
                btnDeletFirstImage.hidden = false;
                
                
            }
                break;
            case 2:
            {
                [arrImageID replaceObjectAtIndex:2 withObject:[NSString stringWithFormat:@"%@",[[arrofImages valueForKey:@"imageId"] objectAtIndex:i]]];
                [_imgTwo sd_setImageWithURL:[NSURL URLWithString:[[arrofImages valueForKey:@"profileImage"] objectAtIndex:i]]
                           placeholderImage:placeHolderImageUser
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                  }];
                btnDeleteSecondImage.hidden = false;
                
                
            }
                break;
            case 3:
            {
                [arrImageID replaceObjectAtIndex:3 withObject:[NSString stringWithFormat:@"%@",[[arrofImages valueForKey:@"imageId"] objectAtIndex:i]]];
                [imgThree sd_setImageWithURL:[NSURL URLWithString:[[arrofImages valueForKey:@"profileImage"] objectAtIndex:i]]
                            placeholderImage:placeHolderImageUser
                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   }];
                btnDeleteThirdImage.hidden = false;
                
                
                
            }
                break;
                
                
            default:
                break;
        }
        
    }
    
    //NSLog(@"%@",arrofImages);
    
}
- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}

@end
