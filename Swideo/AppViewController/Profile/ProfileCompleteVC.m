//
//  ProfileCompleteVC.m
//  Swideo
//
//  Created by akshay on 7/14/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "ProfileCompleteVC.h"

@interface ProfileCompleteVC ()

@end

@implementation ProfileCompleteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width / 2;
    imgProfile.clipsToBounds = YES;    // Do any additional setup after loading the view.
    [imgProfile sd_setImageWithURL:[NSURL URLWithString:[[UserDefaultHelper sharedDefaults] getuserImage]]
              placeholderImage:placeHolderImageUser
                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                     }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionStartBrowsing:(id)sender {
     HomeViewController* home  =[DashStoryBoard instantiateViewControllerWithIdentifier:@"Home"];
    [self.navigationController pushViewController:home animated:YES];

}
@end
