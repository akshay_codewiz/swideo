//
//  ProfileCell.h
//  Swideo
//
//  Created by akshay on 7/13/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPlaceHolderTextView.h"
@interface ProfileCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIPlaceHolderTextView *txtView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
