//
//  InterestListVC.h
//  Swideo
//
//  Created by akshay on 7/13/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol InterestSelectionDelegate;

@interface InterestListVC : UIViewController
{

    __weak IBOutlet UISearchBar *searchBar;
    __weak IBOutlet UITableView *tblView;
    NSMutableArray *arrInterestCopy ;
}
@property (nonatomic ,readwrite) BOOL  isEdit ;
- (IBAction)doneAction:(id)sender;
@property (nonatomic)id InterestSelectiondelegate ;
@property (nonatomic,strong)     NSMutableArray *arrInterest ;

@end
@protocol InterestSelectionDelegate <NSObject>
@optional
//Delegate Methods
-(void)SelectInterestDelegate :(NSMutableArray *)arrSelectedInterest  ;
@end