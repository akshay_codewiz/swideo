//
//  InterestListVC.m
//  Swideo
//
//  Created by akshay on 7/13/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "InterestListVC.h"
#import "AgeCell.h"
@interface InterestListVC ()

@end

@implementation InterestListVC
@synthesize arrInterest  ;
- (void)viewDidLoad {
    [super viewDidLoad];
    if (_isEdit) {
            [self getIterestAPi];
    }
    else{
        if ([arrInterest count] == 0) {
            [self getIterestAPi];

        }
        else{
            arrInterestCopy = [arrInterest mutableCopy];
        }
    }
    
    
    // Do any additional setup after loading the view.
}
#pragma mark - Validate User name
-(void)getIterestAPi
{
    arrInterest = [[NSMutableArray alloc]init];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    if (_isEdit) {
        [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];

    }

    CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,getInterestListAPI] withTag:getInterestListAPITag withParameter:dict];
    apiObject.delegate = self ;
    
}


#pragma mark
#pragma mark - API Delegate
- (void)customURLConnectionDidFinishLoading:(CustomAFNetWorking *)connection withTag:(int)tagCon withResponse:(id)response
{
    [GlobalMethods removeLoadingViewfromView:self.view];

    switch (tagCon) {
        case getInterestListAPITag:
        {
            [GlobalMethods removeLoadingViewfromView:self.view];
            NSDictionary * dictResponse  = (NSDictionary *)response ;

            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {

                arrInterest = [NSMutableArray arrayWithArray:[dictResponse valueForKey:@"data"]];
                arrInterestCopy =[arrInterest mutableCopy];
                [tblView reloadData];
            }


            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];

            }
        }
            break ;
            case updateInterestTag:
        {
            NSDictionary * dictResponse  = (NSDictionary *)response ;
            
            if ([[dictResponse valueForKey:@"success"] boolValue] == true) {
                
                [self.navigationController popViewControllerAnimated:true];
            }
            
            
            else{
                [GlobalMethods showAlertViewToView:self andTitle:@"" andMessage:[dictResponse valueForKey:@"message"]];
                
            }
        }
            break ;
        default:
            break;

    }

}
- (void)customURLConnection:(CustomAFNetWorking *)connection withTag:(int)tagCon didFailWithError:(NSError *)error
{
    [GlobalMethods removeLoadingViewfromView:self.view];
    NSLog(@"%@",error);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrInterest count] ;
    
}

- (CGFloat)tableView:(UITableView *)tableView
estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
#pragma mark
#pragma mark - TableView Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AgeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Age" forIndexPath:indexPath];
    cell.lblGender.text =[[arrInterest objectAtIndex:indexPath.row] valueForKey:@"name"];
    [cell.imgCheck setHidden:true];
    if ([[[arrInterest objectAtIndex:indexPath.row] valueForKey:@"flag"] isEqualToString:@"1"]) {
        [cell.imgCheck setHidden:false];
        
    }
    
       return  cell ;
    
    
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *isLike = [NSString stringWithFormat:@"%@",[[arrInterest objectAtIndex:indexPath.row] valueForKey:@"flag"]];
    if ([isLike isEqualToString:@"0"])
    {
        isLike =@"1";
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
        [dict setValue:[[arrInterest objectAtIndex:indexPath.row] valueForKey:@"name"] forKey:@"name"];
        [dict setValue:isLike forKey:@"flag"];
        [dict setValue:[[arrInterest objectAtIndex:indexPath.row] valueForKey:@"interestId"] forKey:@"interestId"];
        [arrInterestCopy replaceObjectAtIndex:[arrInterestCopy indexOfObject:[arrInterest objectAtIndex:indexPath.row]] withObject:dict];

        [arrInterest replaceObjectAtIndex:indexPath.row withObject:dict];


    }
    else
    {
        isLike =@"0";
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
        [dict setValue:[[arrInterest objectAtIndex:indexPath.row] valueForKey:@"name"] forKey:@"name"];
        [dict setValue:isLike forKey:@"flag"];
        [dict setValue:[[arrInterest objectAtIndex:indexPath.row] valueForKey:@"interestId"] forKey:@"interestId"];
        [arrInterestCopy replaceObjectAtIndex:[arrInterestCopy indexOfObject:[arrInterest objectAtIndex:indexPath.row]] withObject:dict];

        [arrInterest replaceObjectAtIndex:indexPath.row withObject:dict];


    }
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [tblView reloadData];
        
    });
    
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)doneAction:(id)sender {
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"flag ==  %@", @"1"];
    if (_isEdit) {
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
        
        [dict setValue:[[UserDefaultHelper sharedDefaults] getUserID] forKey:@"userId"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"flag ==  %@", @"1"];
        if ([NSMutableArray arrayWithArray:[arrInterest filteredArrayUsingPredicate:predicate]].count > 0) {
            
            [dict setValue:[GlobalMethods getJsonFromArray:[[NSMutableArray arrayWithArray:[NSMutableArray arrayWithArray:[arrInterest filteredArrayUsingPredicate:predicate]]] valueForKey:@"interestId"]] forKey:@"myInterest"];
            
            
        }
        else
        {
            //            NSArray *arr = [[NSArray alloc]init];
            [ dict setValue:@"[]" forKey:@"myInterest"];
            
        }
        
        [GlobalMethods showLoadingViewOnView:self.view :hudMessage];

        CustomAFNetWorking *apiObject  =[[CustomAFNetWorking alloc]initWithPost:[NSString stringWithFormat:@"%@%@",MainAPIUrl,updateUserProfileAPI] withTag:updateInterestTag withParameter:dict];
        apiObject.delegate = self ;

    }
    else{
        [self.InterestSelectiondelegate SelectInterestDelegate:arrInterestCopy ];
        [self.navigationController popViewControllerAnimated:true];

    }
}
#pragma mark - Search Bar Delegate
- (void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText
{
   NSString *strSearchtext = [searchBar.text stringByAppendingString:searchBar1.text];
    if (strSearchtext.length > 0) {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@",searchText];
    arrInterest = [NSMutableArray arrayWithArray:[arrInterestCopy filteredArrayUsingPredicate:predicate]];
    }
    else{
        arrInterest = [arrInterestCopy mutableCopy];
    }
    [tblView reloadData];
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
