//
//  MyProfileViewController.h
//  Swideo
//
//  Created by akshay on 7/13/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface MyProfileViewController : UIViewController <UITextViewDelegate>
{
    __weak IBOutlet UIImageView *imgBig;
    __weak IBOutlet UITableView *tblView;
    NSMutableArray *arrSelInterest ;
    __weak IBOutlet UIImageView *imgOne;
    __weak IBOutlet UIImageView *imgThree;
    int selectedImgeTag ;
    NSMutableArray *arrImageID ;
    __weak IBOutlet UIButton *btnDeleteBigImage;
    __weak IBOutlet UIButton *btnDeletFirstImage;
    __weak IBOutlet UIButton *btnDeleteSecondImage;
    __weak IBOutlet UIButton *btnDeleteThirdImage;
    __weak IBOutlet UIButton *btnNext;
    NSMutableArray *arrDAta ;
    NSString *strAboutME ;
    
}
@property (nonatomic, readwrite)  BOOL isEdit;

@property (weak, nonatomic) IBOutlet UIImageView *imgTwo;
- (IBAction)backAction:(id)sender;
- (IBAction)doneAction:(id)sender;
- (IBAction)actionSelectBigImage:(id)sender;
- (IBAction)actionDeleteBigImage:(id)sender;
- (IBAction)ActionSelectFirstImage:(id)sender;
- (IBAction)actionDeleteFirstImage:(id)sender;
- (IBAction)actionSelectSecondImage:(id)sender;
- (IBAction)actionDeleteSecondImage:(id)sender;
- (IBAction)actionSelectThirdImage:(id)sender;
- (IBAction)actionDeleThirdImage:(id)sender;

@end
