//
//  AkButton.h
//  TextField
//
//  Created by akshay on 6/20/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AkButton : UIButton
@property(nonatomic,readwrite)IBInspectable float roundedCorner;
@property(nonatomic,retain)IBInspectable UIColor *cornerBorderColor;
@property(nonatomic,readwrite)IBInspectable CGFloat cornerBorderWidth;
@property(nonatomic,retain)IBInspectable UIImage *BGImage;
@property(nonatomic,retain)IBInspectable UIImage *SelectedBGImage;
@property(nonatomic,retain)IBInspectable UIImage *HighlightBGImage;
@property(nonatomic,retain)IBInspectable UIColor *HighlightBGColor;
@property(nonatomic,retain)IBInspectable UIColor *SelectedBGColor;
@property(nonatomic,retain)IBInspectable UIColor *BGColor;








@end
