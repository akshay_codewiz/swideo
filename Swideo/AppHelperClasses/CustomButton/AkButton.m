

//
//  AkButton.m
//  TextField
//
//  Created by akshay on 6/20/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "AkButton.h"

@implementation AkButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
}
/*
-(instancetype)initWithFrame:(CGRect)frame{
    
    
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 0.0 ;
        
    }
    return self ;
}
-(CGFloat)roundButton
{
    return  _roundedCorner ;
}
-(void)setCornerRadious :(CGFloat)cornerRadious
{
    _roundedCorner = cornerRadious ;
    self.layer.cornerRadius = _roundedCorner ;

}
 */
- (void)drawRect:(CGRect)rect {
    
    CALayer *layer = [self layer];
    [layer setMasksToBounds:_roundedCorner];
    [layer setCornerRadius:_roundedCorner];
    self.backgroundColor = [UIColor greenColor];
    
    
    // Corner radious
    self.layer.borderWidth = self.cornerBorderWidth ;
    self.layer.borderColor = self.cornerBorderColor.CGColor;
    // BG Images
    [self setBackgroundImage:self.BGImage forState:UIControlStateNormal];
    [self setBackgroundImage:self.SelectedBGImage forState:UIControlStateSelected];
    [self setBackgroundImage:self.HighlightBGImage forState:UIControlStateHighlighted];
    
    // Color
    [self setTitleColor:self.backgroundColor forState:UIControlStateNormal];
    [self setTitleColor:self.SelectedBGColor forState:UIControlStateSelected];
    [self setTitleColor:self.HighlightBGColor forState:UIControlStateHighlighted];


    [super drawRect:rect];
    
    
    
}
//-(void)setSelected:(BOOL)selected{
//    self.selected = !self.selected ;
//    
//}

@end
