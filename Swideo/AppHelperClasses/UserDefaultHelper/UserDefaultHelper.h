//
//  UserDefaultHelper.h
//  PALS
//
//  Created by Gaurav Parmar on 15/03/16.
//  Copyright © 2016 Quantum Technolabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaultHelper : NSObject

+(UserDefaultHelper *)sharedDefaults;

- (void)setAccessToken:(NSString *)token;
- (NSString *)getAccessToken;
- (void)isLogin:(NSString *)login;
- (NSString *)getISLoggedIn;
- (void)setUserID:(NSString *)ID;
- (NSString *)getUserID;
- (void)setFirstName:(NSString *)firstName;
- (NSString *)getFirstName;
- (void)setLastName:(NSString *)lastName;
- (NSString *)getLastName;
- (void)setEmail:(NSString *)email;
- (NSString *)getEmail;
- (void)setUserName:(NSString *)userName;
- (NSString *)getPassword;
- (void)setPassword:(NSString *)userPassword;
- (NSString *)getuserImage;
- (void)setUserImage:(NSString *)userImage;

- (NSString *)getUserName;

- (NSString *)isRegister;
- (void)isRegister:(NSString *)userPassword;
- (NSString *)isProfileCompleted;
- (void)ProfileCompleted:(NSString *)ProfileCompleted;
- (NSString *)isHaveFilterQueries;
- (void)isHaveFilterQueries:(NSString *)isHaveFilter;

- (NSString *)isGhostMode;
- (void)isGhostMode:(NSString *)isGhostMode;

// Remove values
- (void)removeISLoggedIn ;
- (void)removeAccessToken ;
- (void)removeUserID ;
- (void)removeFirstName ;
- (void)removeLastName ;
- (void)removeEmail ;
- (void)removePassword ;


- (void)resetDefaults ;
 
@end
