//
//  UserDefaultHelper.m
//  PALS
//
//  Created by Gaurav Parmar on 15/03/16.
//  Copyright © 2016 Quantum Technolabs. All rights reserved.
//

#import "UserDefaultHelper.h"


static UserDefaultHelper *sharedDefaults;
@implementation UserDefaultHelper

+ (UserDefaultHelper *)sharedDefaults
{
    if(!sharedDefaults)
    {
        sharedDefaults = [[UserDefaultHelper alloc]init];
    }
    return sharedDefaults;
}


- (void)isLogin:(NSString *)login
{
    [[NSUserDefaults standardUserDefaults] setValue:login forKey:isLoggedIN];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)getISLoggedIn
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:isLoggedIN];
}
- (void)removeISLoggedIn
{
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:isLoggedIN];
}
- (void)removeisRegister{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:ISREGISTER];
    
}
- (void)removeisisHaveFilterQueries{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:ISHAVESEARCH];
    
}
- (void)removeuserImage
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:userImageURL];
    
}
- (void)removeisProfileCompleted{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:ISPROFILECOMPLETE];
    
}
- (void)setAccessToken:(NSString *)token
{
    [[NSUserDefaults standardUserDefaults] setValue:token forKey:AccessToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)getAccessToken
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:AccessToken];
}
- (void)removeAccessToken
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:AccessToken];
}
- (void)setUserID:(NSString *)ID
{
    [[NSUserDefaults standardUserDefaults] setValue:ID forKey:userID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)getUserID
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:userID];
}
- (void)removeUserID
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:userID];
}


- (void)setFirstName:(NSString *)firstName
{
    [[NSUserDefaults standardUserDefaults] setValue:firstName forKey:FIRSTNAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)getFirstName
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:FIRSTNAME];
}
- (void)removeFirstName
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:FIRSTNAME];
}

- (void)setLastName:(NSString *)lastName
{
    [[NSUserDefaults standardUserDefaults] setValue:lastName forKey:LASTNAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)getLastName
{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:LASTNAME]) {
        return [[NSUserDefaults standardUserDefaults] valueForKey:LASTNAME];
    }
    return @"";
}
- (void)removeLastName
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LASTNAME];

}

- (void)setEmail:(NSString *)email
{
    [[NSUserDefaults standardUserDefaults] setValue:email forKey:EMAIL];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)getEmail
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:EMAIL];
}
- (void)removeEmail
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:EMAIL];
    
}

- (void)setUserName:(NSString *)userName
{
    [[NSUserDefaults standardUserDefaults] setValue:userName forKey:USERNNAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)getUserName
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:USERNNAME];

}
- (void)removeUserName
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USERNNAME];
    
}
- (NSString *)getPassword
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:PASSWORD];

}
- (void)setPassword:(NSString *)userPassword
{
    [[NSUserDefaults standardUserDefaults] setValue:userPassword forKey:PASSWORD];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)removePassword
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PASSWORD];
    
}
- (NSString *)getuserImage
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:userImageURL];

}
- (void)setUserImage:(NSString *)userImage;
{
    [[NSUserDefaults standardUserDefaults] setValue:userImage forKey:userImageURL];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
- (NSString *)isRegister
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:ISREGISTER];
    
}
- (void)isRegister:(NSString *)isvalue
{
    [[NSUserDefaults standardUserDefaults] setValue:isvalue forKey:ISREGISTER];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (NSString *)isProfileCompleted
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:ISPROFILECOMPLETE];

}
- (void)ProfileCompleted:(NSString *)ProfileCompleted
{
    [[NSUserDefaults standardUserDefaults] setValue:ProfileCompleted forKey:ISPROFILECOMPLETE];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
- (NSString *)isHaveFilterQueries
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:ISHAVESEARCH];
    
}
- (void)isHaveFilterQueries:(NSString *)isHaveFilter{
    
    [[NSUserDefaults standardUserDefaults] setValue:isHaveFilter forKey:ISHAVESEARCH];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (void)resetDefaults
{
    [self removeEmail];
    [self removeUserID];
    [self removeUserName];
    [self removeLastName];
    [self removePassword];
    [self removeFirstName];
    [self removeISLoggedIn];
    [self removeisRegister];
    [self removeisisHaveFilterQueries];
    [self removeuserImage];
    [self removeisProfileCompleted];
}

- (NSString *)isGhostMode
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:ISGHOSTMODE];

}
- (void)isGhostMode:(NSString *)isGhostMode
{
    [[NSUserDefaults standardUserDefaults] setValue:isGhostMode forKey:ISGHOSTMODE];
    [[NSUserDefaults standardUserDefaults] synchronize];

}@end
