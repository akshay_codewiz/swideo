//
//  GlobalMethods.m
//  PALS
//
//  Created by Gaurav Parmar on 15/03/16.
//  Copyright © 2016 Quantum Technolabs. All rights reserved.
//

#import "GlobalMethods.h"

@implementation GlobalMethods
#pragma mark string null check
+ (NSString *) getNotNullString:(NSString *) aString {
    
    if ((NSNull *) aString == [NSNull null]) {
        return @"";
    }
    if ([aString containsString:@"(null)"]) {
        return @"";
    }
    
    if (aString == nil) {
        return @"";
    } else if ([aString length] == 0) {
        return @"";
    } else {
        aString = [aString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([aString length] == 0) {
            return @"";
        }
    }
    
    return aString;
}

#pragma mark
#pragma mark - Loader View
//Loading View Methods
+ (void) showLoadingViewOnView:(UIView*)view
{
    [MBProgressHUD showHUDAddedTo:view animated:YES];
}
+ (void) showLoadingViewOnView:(UIView*)view :(NSString *)text
{
    [MBProgressHUD showHUDAddedTo:view animated:YES text:text];
}
+ (void) removeLoadingViewfromView:(UIView*)view
{
    [MBProgressHUD hideHUDForView:view animated:YES];
}

#pragma mark
#pragma mark - Validation For Valid E-Mail
+(BOOL) IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark
#pragma mark Date Conversion
-(NSString *)dateStringFromDate:(NSDate *)selectedDate
{
    NSDateFormatter *dateFormate = [[NSDateFormatter alloc] init];
    [dateFormate setDateFormat:@"MMM dd,yyyy"];
    return [dateFormate stringFromDate:selectedDate];
}
-(NSDate *)dateFromDateString:(NSString *)dateString
{
    NSDateFormatter *dateFormate = [[NSDateFormatter alloc] init];
    [dateFormate setDateFormat:@"MMM dd,yyyy"];
    [dateFormate setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    return [dateFormate dateFromString:dateString];
}
-(NSString *)dateStringFromTime:(NSDate *)selectedDate
{
    NSDateFormatter *dateFormate = [[NSDateFormatter alloc] init];
    [dateFormate setDateFormat:@"hh:mm aa"];
    return [dateFormate stringFromDate:selectedDate];
}
-(NSString *)dateStringFromTime24:(NSDate *)selectedDate
{
    NSDateFormatter *dateFormate = [[NSDateFormatter alloc] init];
    [dateFormate setDateFormat:@"HH:mm:ss"];
    return [dateFormate stringFromDate:selectedDate];
}

#pragma mark - Alert View Methods
+(void)showAlertViewToView :(UIViewController *)VC  andTitle:(NSString *)title andMessage:(NSString *)message
{
    [VC.view makeToast:message duration:2.0 position:CSToastPositionCenter];
}

#pragma mark base 64 encoded string
+ (NSString*)encodeStringTo64:(NSString*)fromString ;{
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String;
    if ([plainData respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        base64String = [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
    } else {
        base64String = [plainData base64Encoding];                              // pre iOS7
    }
    
    return base64String;
}
+ (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}
-(void)setUpData
{
    
   NSMutableArray *arrLike =[[NSMutableArray alloc] init];
    NSMutableDictionary * dict =[[NSMutableDictionary alloc]init];
    
    dict =[[NSMutableDictionary alloc]init];
    
    [dict setValue:@"Profiel" forKey:@"title"];
    [dict setValue:@"0" forKey:@"islike"];
    [arrLike addObject:dict];
    
    dict =[[NSMutableDictionary alloc]init];
    [dict setValue:@"HoptSpots" forKey:@"title"];
    [dict setValue:@"0" forKey:@"islike"];
    // [arrLike addObject:dict];
    
    
    dict =[[NSMutableDictionary alloc]init];
    [dict setValue:@"Push notificatie" forKey:@"title"];
    [dict setValue:@"0" forKey:@"islike"];
    [arrLike addObject:dict];
    
    
    dict =[[NSMutableDictionary alloc]init];
    [dict setValue:@"Uitloggen" forKey:@"title"];
    [dict setValue:@"0" forKey:@"islike"];
    [arrLike addObject:dict];
    
    
}
+(NSString *)getJsonFromArray :(NSArray *)arrValue
{
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:arrValue options:NSJSONWritingPrettyPrinted error:nil];
    return [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
}
+(void)pushViewcontrollerInSearchStoriboardTo: (UIViewController *)viewcontroller andIndentifier :(NSString *)VCidentfier wintNavigation :(UINavigationController *)navigationController{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Search" bundle:nil];
    viewcontroller =[storyboard instantiateViewControllerWithIdentifier:VCidentfier];
    [navigationController pushViewController:viewcontroller animated:YES ];

}
+(void)pushViewcontrollerInSearchStoriboardTo: (UIViewController *)viewcontroller andIndentifier :(NSString *)VCidentfier
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Search" bundle:nil];
    viewcontroller =[storyboard instantiateViewControllerWithIdentifier:VCidentfier];
    [viewcontroller.navigationController pushViewController:viewcontroller animated:YES ];
}
+(void)pushViewcontrollerMessageStoriboardTo: (UIViewController *)viewcontroller andIndentifier :(NSString *)VCidentfier wintNavigation :(UINavigationController *)navigationController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Message" bundle:nil];
    viewcontroller =[storyboard instantiateViewControllerWithIdentifier:VCidentfier];
    [viewcontroller.navigationController pushViewController:viewcontroller animated:YES ];
    
}
+(void)pushViewcontrollerProfileStoriboardTo: (UIViewController *)viewcontroller andIndentifier :(NSString *)VCidentfier wintNavigation :(UINavigationController *)navigationController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Profile" bundle:nil];
    viewcontroller =[storyboard instantiateViewControllerWithIdentifier:VCidentfier];
    [navigationController pushViewController:viewcontroller animated:YES ];
    
}
+(void)pushViewcontrollerMainStoriboardTo: (UIViewController *)viewcontroller andIndentifier :(NSString *)VCidentfier wintNavigation :(UINavigationController *)navigationController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    viewcontroller =[storyboard instantiateViewControllerWithIdentifier:VCidentfier];
    [navigationController pushViewController:viewcontroller animated:YES ];

}
-(void)showCRToast :(NSString *)strUser andMessage :(NSString *)strMessage
{
    NSDictionary *options = @{
                              kCRToastTextKey : [NSString stringWithFormat:@"%@ : %@",strUser,strMessage],
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColor orangeColor],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
                              };
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                }];
}
+(BOOL)checkImagePermission
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        return true ;
        // do your logic
    } else if(authStatus == AVAuthorizationStatusDenied){
        return false ;
        // denied
    } else if(authStatus == AVAuthorizationStatusRestricted){
        return false ;

        // restricted, normally won't happen
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        // not determined?!
        return true ;

//        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
//            if(granted){
//                
//            } else {
//            }
//        }];
    }
    
    return false ;
}
+ (NSString *)extractNumberFromText:(NSString *)text
{
    NSCharacterSet *nonDigitCharacterSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    NSString *number =[[text componentsSeparatedByCharactersInSet:nonDigitCharacterSet] componentsJoinedByString:@""];
    return number;
}
+(NSArray *)getArrayFromString :(NSString *)jsonString
{
    NSArray *jsonArray =[[NSArray alloc] init];
    if (jsonString.length > 0) {
        NSData *myNSData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error ;
        jsonArray = [NSJSONSerialization JSONObjectWithData:myNSData options:kNilOptions error:&error];
        
        if (error != nil) {
            NSLog(@"Error parsing JSON.");
            return jsonArray ;
        }
        else {
            NSLog(@"Array: %@", jsonArray);
            return jsonArray ;
        }

    }
    else{
        return jsonArray ;

    }
    

}
@end
