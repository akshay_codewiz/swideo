//
//  AkTextfield.m
//  TextField
//
//  Created by akshay on 6/18/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import "AkTextfield.h"

@implementation AkTextfield

-(void)awakeFromNib
{

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect {
    
    
    if (self.iconImageView == nil) {
        self.iconImageView = [[UIImageView alloc] init];
        self.iconImageView.frame = CGRectMake(10, (self.frame.size.height - self.iconImageHieght)/2, self.iconImageWidth, self.iconImageHieght);

        
    }
    [self setValue:self.placeHolderColor forKeyPath:@"_placeholderLabel.textColor"];
    self.iconImageView.image = self.iconImage ;
    [self addSubview:_iconImageView];
    self.layer.cornerRadius = _roundedCorner ;
    self.layer.masksToBounds = true ;
    [super drawRect:rect];
    


}
-(CGRect) textRectForBounds:(CGRect)bounds
{
    return UIEdgeInsetsInsetRect([super textRectForBounds:bounds], UIEdgeInsetsMake(self.insetsTop,10 + self.iconImageWidth+ self.insetsLeft, self.insetsBottom, self.insetsRight));
}
-(CGRect) editingRectForBounds:(CGRect)bounds
{
    return UIEdgeInsetsInsetRect([super textRectForBounds:bounds], UIEdgeInsetsMake(self.insetsTop,10+ self.iconImageWidth+ self.insetsLeft , self.insetsBottom, self.insetsRight));
}
@end
