//
//  AkTextfield.h
//  TextField
//
//  Created by akshay on 6/18/16.
//  Copyright © 2016 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface AkTextfield : UITextField
@property(nonatomic,retain) UIImageView *iconImageView;
@property(nonatomic,retain)IBInspectable UIColor *placeHolderColor;
@property(nonatomic,readwrite)IBInspectable float roundedCorner;
@property(nonatomic,readwrite)IBInspectable float iconImageWidth;
@property(nonatomic,readwrite)IBInspectable float iconImageHieght;
@property(nonatomic,retain)IBInspectable UIImage *iconImage;
@property (nonatomic,readwrite)IBInspectable CGFloat insetsTop ;
@property (nonatomic,readwrite)IBInspectable CGFloat insetsBottom ;
@property (nonatomic,readwrite)IBInspectable CGFloat insetsLeft;
@property (nonatomic,readwrite)IBInspectable CGFloat insetsRight ;





@end
