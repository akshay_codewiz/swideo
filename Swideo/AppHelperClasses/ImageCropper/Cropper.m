//
//  Cropper.m
//  DemoCropping
//
//  Created by Gaurav Parmar on 22/09/15.
//  Copyright (c) 2015 qtm. All rights reserved.
//

#import "Cropper.h"

@interface Cropper ()

@end

@implementation Cropper

@synthesize delegate = _delegate;
@synthesize image = _image;
@synthesize cropSize = _cropSize;
@synthesize rescaleImage = _rescaleImage;
@synthesize rescaleFactor = _rescaleFactor;
@synthesize dismissAnimated = _dismissAnimated;
@synthesize overlayColor = _overlayColor;
@synthesize innerBorderColor = _innerBorderColor;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupLayout];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupLayout
{
    self.view.backgroundColor = [UIColor blackColor];
    
//    self.navigationController.navigationBarHidden= NO;
    
    double navBarHeight = self.navigationController.navigationBar.frame.size.height;
    double frameWidth = self.view.frame.size.width;
    double frameHeight = self.view.frame.size.height-navBarHeight;
    CGFloat imageWidth = CGImageGetWidth(self.image.CGImage);
    CGFloat imageHeight = CGImageGetHeight(self.image.CGImage);
    float scaleX = self.cropSize.width / imageWidth;
    float scaleY = self.cropSize.height / imageHeight;
    float scaleScroll =  (scaleX < scaleY ? scaleY : scaleX);
    
    scrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    scrollView.bounds = CGRectMake(0, 0,imageWidth, imageHeight);
    scrollView.frame = CGRectMake(0, 0, frameWidth, frameHeight);
    scrollView.delegate = self;
    scrollView.scrollEnabled = YES;
    scrollView.contentSize = self.image.size;
    scrollView.pagingEnabled = NO;
    scrollView.directionalLockEnabled = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    //Limit zoom
    scrollView.maximumZoomScale = scaleScroll*10.;
    scrollView.minimumZoomScale = scaleScroll;
    [self.view addSubview:scrollView];
    
    imageView = [[UIImageView alloc] initWithImage:self.image];
    [scrollView addSubview:imageView];
    
    [UIColor colorWithRed:0/255. green:140/255. blue:190/255. alpha:1];
    
    // **********************************************
    // * Create top shaded overlay
    // **********************************************
    UIImageView *overlayTop = [[UIImageView alloc] initWithFrame:CGRectMake(0., 0., frameWidth, frameHeight/2.-self.cropSize.height/2.)];
    overlayTop.backgroundColor = self.overlayColor;
    [self.view addSubview:overlayTop];
    
    // **********************************************
    // * Create bottom shaded overlay
    // **********************************************
    UIImageView *overlayBottom = [[UIImageView alloc] initWithFrame:CGRectMake(0., frameHeight/2.+self.cropSize.height/2, frameWidth, frameHeight/2.-self.cropSize.height/2.)];
    overlayBottom.backgroundColor = self.overlayColor;
    [self.view addSubview:overlayBottom];
    
    // **********************************************
    // * Create left shaded overlay
    // **********************************************
    UIImageView *overlayLeft = [[UIImageView alloc] initWithFrame:CGRectMake(0., frameHeight/2.-self.cropSize.height/2., frameWidth/2.-self.cropSize.width/2., self.cropSize.height)];
    overlayLeft.backgroundColor = self.overlayColor;
    [self.view addSubview:overlayLeft];
    
    // **********************************************
    // * Create right shaded overlay
    // **********************************************
    UIImageView *overlayRight = [[UIImageView alloc] initWithFrame:CGRectMake(frameWidth/2.+self.cropSize.width/2., frameHeight/2.-self.cropSize.height/2., frameWidth/2.-self.cropSize.width/2., self.cropSize.height)];
    overlayRight.backgroundColor = self.overlayColor;
    [self.view addSubview:overlayRight];
    
    // **********************************************
    // * Create inner border overlay
    // **********************************************
    UIImageView *overlayInnerBorder = [[UIImageView alloc] initWithFrame:CGRectMake(frameWidth/2.-self.cropSize.width/2., frameHeight/2.-self.cropSize.height/2., self.cropSize.width, self.cropSize.height)];
    overlayInnerBorder.backgroundColor = [UIColor clearColor];
    overlayInnerBorder.layer.masksToBounds = YES;
    overlayInnerBorder.layer.borderColor = self.innerBorderColor.CGColor;
    overlayInnerBorder.layer.borderWidth = 1;
    [self.view addSubview:overlayInnerBorder];
    
    // **********************************************
    // * Set scroll view inset so that corners of images can be accessed
    // **********************************************
    scrollView.contentInset = UIEdgeInsetsMake(overlayTop.frame.size.height, overlayLeft.frame.size.width, overlayBottom.frame.size.height, overlayRight.frame.size.width);
    
    // **********************************************
    // * Add gesture recognizer for single tap to display image rotation menu
    // **********************************************
//    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
//    [scrollView addGestureRecognizer:singleTap];
    
    // **********************************************
    // * Set initial zoom of scroll view
    // **********************************************
    [scrollView setZoomScale:scaleScroll animated:NO];
    
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeSystem];
    btnDone.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-50, [UIScreen mainScreen].bounds.size.height-30, 50, 20);
    btnDone.layer.borderColor= [UIColor redColor].CGColor;
    btnDone.layer.borderWidth = 2.0;
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnDone];
}
-(id)initWithImage:(UIImage*)theImage withCropSize:(CGSize)theSize willRescaleImage:(BOOL)willRescaleImage withRescaleFactor:(double)theFactor willDismissAnimated:(BOOL)willDismissAnimated
{
    if(self)
    {
        self = [super init];
        self.cropSize = theSize;
        self.rescaleImage = willRescaleImage;
        self.rescaleFactor = theFactor;
        self.dismissAnimated = willDismissAnimated;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // **********************************************
        // * Set default parameters
        // **********************************************
        self.image = [[UIImage alloc] init];
        self.cropSize = CGSizeMake(320.0,320.0);
        self.rescaleImage = YES;
        self.rescaleFactor = 1.0;
        self.dismissAnimated = YES;
        self.overlayColor = [UIColor colorWithRed:0/255. green:0/255. blue:0/255. alpha:0.7];
        self.innerBorderColor = [UIColor colorWithRed:255./255. green:255./255. blue:255./255. alpha:0.7];
    }
    return self;
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return imageView;
}

-(void)doneButton
{
    double cropAreaHorizontalOffset = self.view.frame.size.width/2.-self.cropSize.width/2.;
    double cropAreaVerticalOffset = self.view.frame.size.height/2.-self.cropSize.height/2.;
    CGRect cropRect;
    float scale = 1.0f/scrollView.zoomScale;
    cropRect.origin.x = (scrollView.contentOffset.x+cropAreaHorizontalOffset) * scale;
    cropRect.origin.y = (scrollView.contentOffset.y+cropAreaVerticalOffset) * scale;
    cropRect.size.width = self.cropSize.width * scale;
    cropRect.size.height = self.cropSize.height * scale;
    
    // **********************************************
    // * Crop image
    // **********************************************
    self.image = imageFromView(self.image, &cropRect);
    
    // **********************************************
    // * Resize image if willRescaleImage == YES
    // **********************************************
//    if (self.rescaleImage)
//    {
//        self.image = [self resizedImage:CGSizeMake(self.cropSize.width*self.rescaleFactor,self.cropSize.height*self.rescaleFactor) interpolationQuality:kCGInterpolationDefault];
//    }

    [self dismissViewControllerAnimated:self.dismissAnimated completion:nil];
    [self.delegate imageCropperDidFinish:self withImage:self.image];
}

UIImage* imageFromView(UIImage* srcImage, CGRect* rect)
{
//    UIImage *fixOrientation = [srcImage fixOrientation];
//    CGImageRef cr = CGImageCreateWithImageInRect(fixOrientation.CGImage, *rect);
    CGImageRef cr = CGImageCreateWithImageInRect(srcImage.CGImage, *rect);
    UIImage* cropped = [UIImage imageWithCGImage:cr];
    CGImageRelease(cr);
    return cropped;
}
//- (UIImage *)resizedImage:(CGSize)newSize interpolationQuality:(CGInterpolationQuality)quality {
//    BOOL drawTransposed;
//    
//    switch (self.imageOrientation) {
//        case UIImageOrientationLeft:
//        case UIImageOrientationLeftMirrored:
//        case UIImageOrientationRight:
//        case UIImageOrientationRightMirrored:
//            drawTransposed = YES;
//            break;
//            
//        default:
//            drawTransposed = NO;
//    }
//    
//    return [self resizedImage:newSize
//                    transform:[self transformForOrientation:newSize]
//               drawTransposed:drawTransposed
//         interpolationQuality:quality];
//}
//- (UIImage *)fixOrientation
//{
//    if (self.imageOrientation == UIImageOrientationUp) return self;
//    
//    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
//    [self drawInRect:(CGRect){0, 0, self.size}];
//    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return normalizedImage;
//}
//// Returns an affine transform that takes into account the image orientation when drawing a scaled image
//- (CGAffineTransform)transformForOrientation:(CGSize)newSize {
//    CGAffineTransform transform = CGAffineTransformIdentity;
//    
//    switch (self.imageOrientation)
//    {
//        case UIImageOrientationDown:           // EXIF = 3
//        case UIImageOrientationDownMirrored:   // EXIF = 4
//            transform = CGAffineTransformTranslate(transform, newSize.width, newSize.height);
//            transform = CGAffineTransformRotate(transform, M_PI);
//            break;
//            
//        case UIImageOrientationLeft:           // EXIF = 6
//        case UIImageOrientationLeftMirrored:   // EXIF = 5
//            transform = CGAffineTransformTranslate(transform, newSize.width, 0);
//            transform = CGAffineTransformRotate(transform, M_PI_2);
//            break;
//            
//        case UIImageOrientationRight:          // EXIF = 8
//        case UIImageOrientationRightMirrored:  // EXIF = 7
//            transform = CGAffineTransformTranslate(transform, 0, newSize.height);
//            transform = CGAffineTransformRotate(transform, -M_PI_2);
//            break;
//    }
//    
//    switch (self.imageOrientation) {
//        case UIImageOrientationUpMirrored:     // EXIF = 2
//        case UIImageOrientationDownMirrored:   // EXIF = 4
//            transform = CGAffineTransformTranslate(transform, newSize.width, 0);
//            transform = CGAffineTransformScale(transform, -1, 1);
//            break;
//            
//        case UIImageOrientationLeftMirrored:   // EXIF = 5
//        case UIImageOrientationRightMirrored:  // EXIF = 7
//            transform = CGAffineTransformTranslate(transform, newSize.height, 0);
//            transform = CGAffineTransformScale(transform, -1, 1);
//            break;
//    }
//    
//    return transform;
//}

@end
