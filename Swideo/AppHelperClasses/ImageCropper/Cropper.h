//
//  Cropper.h
//  DemoCropping
//
//  Created by Gaurav Parmar on 22/09/15.
//  Copyright (c) 2015 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CropperDelegate;

@interface Cropper : UIViewController<UIScrollViewDelegate>
{
    UIImage *image;
    id<CropperDelegate> delegate;
    CGSize cropSize;
    BOOL rescaleImage;
    double rescaleFactor;
    BOOL dismissAnimated;
    UIColor *overlayColor;
    UIColor *innerBorderColor;
    
    UIScrollView *scrollView;
    UIImageView *imageView;
}
@property (nonatomic, assign) id<CropperDelegate> delegate;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic) CGSize cropSize;
@property (nonatomic) BOOL rescaleImage;
@property (nonatomic) double rescaleFactor;
@property (nonatomic) BOOL dismissAnimated;
@property (nonatomic, retain) UIColor *overlayColor;
@property (nonatomic, retain) UIColor *innerBorderColor;

-(id)initWithImage:(UIImage*)theImage withCropSize:(CGSize)theSize willRescaleImage:(BOOL)willRescaleImage withRescaleFactor:(double)theFactor willDismissAnimated:(BOOL)willDismissAnimated;
@end

@protocol CropperDelegate <NSObject>

- (void)imageCropperDidFinish:(Cropper *)imageCropper withImage:(UIImage *)image;

@end
