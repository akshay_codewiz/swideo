//
//  AppConstant.h
//  PALS
//
//  Created by Gaurav Parmar on 15/03/16.
//  Copyright © 2016 Quantum Technolabs. All rights reserved.
//




#define AppURLTypeFB @"1"
#define AppURLTypeLinkedIn @"2"

#ifndef AppConstant_h
#define AppConstant_h
#import "AppDelegate.h"

#define APPNAME @"Pals"
//#define XMPPSERVER @"@problempie.co.uk"
#define XMPPSERVER @"@52.208.58.179"
//#define XMPPSERVER @"@facy.mobi"
#define XMPPPREFIX @"pal_"


#define AppFONTName  @""
#define appVerdana @""Y
#define appVerdanaBold @""
#define placeHolderImageUser  [UIImage imageNamed:@"profile_pic_ic"]
#define NoDataText @"No data found."
#define hudMessage @"Loading..."


//UserDefault Keys
#define AccessToken @"accessToken"
#define isLoggedIN @"islogin"
#define userID @"userID"
#define FIRSTNAME @"firstName"
#define LASTNAME @"lastName"
#define EMAIL @"email"
#define USERNNAME @"userName"
#define PASSWORD @"password"
#define userImageURL @"userImage"
#define ISREGISTER @"register"
#define ISPROFILECOMPLETE @"isProfileCompeted"
#define ISHAVESEARCH @"isHaveFilterQueries"
#define ISGHOSTMODE @"isGhostmode"

#define XMPPPAssword @"123456"

#define HeritageType @"1"
#define EyeColorType @"2"
#define HairColorType @"3"
#define HeightType @"4"
#define GenderType @"5"




#define APPCOLOR  [UIColor colorWithRed:(235/255.0) green:(84/255.0) blue:(62/255.0) alpha:1] ;

#define IS_IPHONE_4 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)480) < DBL_EPSILON)
#define IS_IPHONE_5 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)568) < DBL_EPSILON)
#define IS_IPHONE_6 (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)667) < DBL_EPSILON)
#define IS_IPHONE_6_PLUS (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)736) < DBL_EPSILON)


#define DEVICE_SIZE ([[[[UIApplication sharedApplication] keyWindow] rootViewController].view convertRect:[[UIScreen mainScreen] bounds] fromView:nil].size)



#define APPDELEGATE ((AppDelegate*)[UIApplication sharedApplication].delegate)
#define BlankCharacter [NSCharacterSet whitespaceCharacterSet]
#define GLOBALMETHOD [[GlobalMethods alloc]init]
#define PLACEHOLDER [UIColor colorWithRed:(72.0/255.0) green:(72.0/255.0) blue:(72.0/255.0) alpha:1.0]
#define sendMessageNotification @"sendMessageNotification"


#define MainAPIUrl @"http://endolit.com/palsDev/index.php/api/"   //local

//#define MainAPIUrl @"http://192.168.3.15/pals/index.php/api/"   //Live

//#define MainAPIUrl @"http://endolit.com/pals/api/"


#define getInterestList @"interestLists"
#define setInterestList @"userIntrests"
#define validateUserEmail @"validateEmailUserName"
#define userRegistration @"userRegistration"
#define userImportRegistrationData @"userImportRegistrationData"
#define userProfileSet @"userProfileImage"
#define userLogin @"userLogin"
#define forgotPassword @"forgotPassword"
#define getCategoryList @"getCategory"
#define SetUserQuery @"saveUserSearch"
#define GetUserSearchList @"getUserSearchLists"
#define GetSearchResult @"getSearchResult"
#define UpdateUserProfile @"updateProfile"
#define GetZodiacSign @"zodiacLists"
#define GetUserProfile @"getUserProfile"
#define GetOccupation @"userSaveOccupation"
#define getUserUserOccupation @"userGetOccupation"
#define logOutUser @"userLogout"
#define getEducationList @"getUserEducations"
#define getInstitueList @"getInstitueLists"
#define getCourseList @"getCoursesLists"
#define getEducation @"getEducationType"
#define saveEducation @"saveEducations"
#define removeEducation @"removeEducation"
#define removeSearch @"deleteUserSearch"
#define removeOccupation @"removeOccupation"
#define friendRequestAPI @"acceptRejectChatRequest"
#define AddToPalsAPI @"addToPals"
#define RemovePals @"removeFromPals"
#define SendChatRequset @"sendChatRequest"
#define ChatUserList @"chatUserList"
#define blockuserList @"getBlockedLists"
#define getPendingListAPI @"requestPendingList"
#define getPalsList @"palsUserLists"
#define unBlockUserAPI @"unblockUser"
#define setProfileAccess @"profileAccess"
#define GhostMdeAPI @"userGhostMode"
#define blockUserAPI @"blockUser"
#define MediaApi @"uploadMedia"
#define StoreNote @"storeNote"
#define RemoveNote @"removeStoreNote"
#define AddremoveFavourite @"addToFavorite"
#define updateUserLocation @"updateLocation"
#define notifyMeAPI @"setNotify"
#define getNotiFyAPi @"getNotify"
#define GetStoredNodeDetailAPI  @"getStoreNoteDetails"
#define mergeContactsAPI  @"mergeContacts"
#define setUserReport @"reportUser" 
#define  sendMailcontact   @"sendMailToContactMerge"
#define tellAFrined @"tellAFriend"
#define getHeritageList @"getHeritage"
#define getEyeColorList @"getEyeColor"
#define getHairColorList @"getHairColor"
#define setUserDetail @"saveDetails"
#define changePasswordAPI @"changePassword"
#define getGenderAPI @"getGender"



// Import Classes
//#import "ListOccupationViewController.h"
#endif /* AppConstant_h */
