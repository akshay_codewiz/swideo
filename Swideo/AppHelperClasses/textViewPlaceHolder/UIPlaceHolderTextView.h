//
//  UIPlaceHolderTextView.h
//  PALS
//
//  Created by akshay on 6/6/16.
//  Copyright © 2016 Gaurav Parmar. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface UIPlaceHolderTextView : UITextView
@property (nonatomic, retain) IBInspectable NSString *placeholder;
@property (nonatomic, retain) IBInspectable UIColor *placeholderColor;

-(void)textChanged:(NSNotification*)notification;
@end
